# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180406050757) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "artists", force: :cascade do |t|
    t.string   "name"
    t.text     "bio"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "user_id"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "instagram"
    t.string   "soundcloud"
    t.string   "website"
    t.string   "label"
    t.string   "manager"
    t.integer  "related_id"
    t.string   "image"
    t.integer  "similar_id"
    t.integer  "similar_ids", default: [],                 array: true
    t.string   "hometown"
    t.string   "pro"
    t.string   "instrument1"
    t.string   "instrument2"
    t.string   "influence1"
    t.string   "influence2"
    t.string   "influence3"
    t.boolean  "is_featured", default: false
    t.boolean  "active",      default: true
  end

  create_table "comments", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree

  create_table "denied_songs", force: :cascade do |t|
    t.string   "title"
    t.string   "desc"
    t.integer  "artist_id"
    t.integer  "genre_id"
    t.integer  "mood_id"
    t.integer  "pace_id"
    t.integer  "era_id"
    t.integer  "key_id"
    t.integer  "tempo_id"
    t.integer  "instrument_id"
    t.integer  "user_id"
    t.text     "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "eras", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "favorited_id"
    t.string   "favorited_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "favorites", ["favorited_type", "favorited_id"], name: "index_favorites_on_favorited_type_and_favorited_id", using: :btree
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id", using: :btree

  create_table "genres", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "instruments", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
    t.text     "desc"
  end

  create_table "keys", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "moods", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "paces", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "icon"
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "purchase_id"
    t.string   "token"
    t.integer  "amount_cents", default: 0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "payments", ["purchase_id"], name: "index_payments_on_purchase_id", using: :btree

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.integer  "artist_id"
    t.integer  "genre_id"
    t.integer  "mood_id"
    t.integer  "pace_id"
    t.integer  "era_id"
    t.integer  "key_id"
    t.integer  "tempo_id"
    t.integer  "instrument_id"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "pg_search_documents", ["artist_id"], name: "index_pg_search_documents_on_artist_id", using: :btree
  add_index "pg_search_documents", ["era_id"], name: "index_pg_search_documents_on_era_id", using: :btree
  add_index "pg_search_documents", ["genre_id"], name: "index_pg_search_documents_on_genre_id", using: :btree
  add_index "pg_search_documents", ["instrument_id"], name: "index_pg_search_documents_on_instrument_id", using: :btree
  add_index "pg_search_documents", ["key_id"], name: "index_pg_search_documents_on_key_id", using: :btree
  add_index "pg_search_documents", ["mood_id"], name: "index_pg_search_documents_on_mood_id", using: :btree
  add_index "pg_search_documents", ["pace_id"], name: "index_pg_search_documents_on_pace_id", using: :btree
  add_index "pg_search_documents", ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree
  add_index "pg_search_documents", ["tempo_id"], name: "index_pg_search_documents_on_tempo_id", using: :btree

  create_table "playlist_entries", force: :cascade do |t|
    t.integer  "song_id"
    t.integer  "playlist_id"
    t.integer  "order"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "playlist_entries", ["playlist_id"], name: "index_playlist_entries_on_playlist_id", using: :btree
  add_index "playlist_entries", ["song_id"], name: "index_playlist_entries_on_song_id", using: :btree

  create_table "playlists", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "image"
  end

  add_index "playlists", ["user_id"], name: "index_playlists_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.string   "image"
    t.string   "author"
    t.string   "desc"
    t.string   "keyword"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "projects", ["user_id"], name: "index_projects_on_user_id", using: :btree

  create_table "projects_songs", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "song_id"
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "projects_songs", ["project_id"], name: "index_projects_songs_on_project_id", using: :btree
  add_index "projects_songs", ["song_id"], name: "index_projects_songs_on_song_id", using: :btree
  add_index "projects_songs", ["user_id"], name: "index_projects_songs_on_user_id", using: :btree

  create_table "purchase_items", force: :cascade do |t|
    t.integer  "purchase_id"
    t.integer  "song_id"
    t.string   "license"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "downloads",      default: 0
  end

  add_index "purchase_items", ["purchase_id"], name: "index_purchase_items_on_purchase_id", using: :btree
  add_index "purchase_items", ["song_id"], name: "index_purchase_items_on_song_id", using: :btree

  create_table "purchases", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "purchased_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.float    "used_credits", default: 0.0
  end

  add_index "purchases", ["user_id"], name: "index_purchases_on_user_id", using: :btree

  create_table "quotes", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shopping_cart_items", force: :cascade do |t|
    t.integer  "shopping_cart_id"
    t.integer  "song_id"
    t.string   "license"
    t.integer  "price_cents",      default: 0,     null: false
    t.string   "price_currency",   default: "USD", null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "shopping_cart_items", ["shopping_cart_id"], name: "index_shopping_cart_items_on_shopping_cart_id", using: :btree
  add_index "shopping_cart_items", ["song_id"], name: "index_shopping_cart_items_on_song_id", using: :btree

  create_table "shopping_carts", force: :cascade do |t|
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.float    "used_credits", default: 0.0
  end

  create_table "similar_artists", force: :cascade do |t|
    t.integer  "artist_id"
    t.integer  "similar_artist_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "similar_artists", ["artist_id"], name: "index_similar_artists_on_artist_id", using: :btree
  add_index "similar_artists", ["similar_artist_id"], name: "index_similar_artists_on_similar_artist_id", using: :btree

  create_table "songs", force: :cascade do |t|
    t.string   "title"
    t.string   "desc"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "artist_id"
    t.integer  "genre_id"
    t.integer  "mood_id"
    t.integer  "pace_id"
    t.integer  "era_id"
    t.integer  "key_id"
    t.integer  "tempo_id"
    t.integer  "instrument_id"
    t.integer  "user_id"
    t.string   "image"
    t.string   "sample"
    t.float    "duration"
    t.integer  "play_count",      default: 0
    t.integer  "favorites_count", default: 0
    t.boolean  "approved",        default: false
  end

  create_table "tempos", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
    t.text     "desc"
  end

  create_table "user_credits", force: :cascade do |t|
    t.string   "email"
    t.float    "total_credits_till_date", default: 0.0
    t.float    "used_credits",            default: 0.0
    t.float    "remaining_credits",       default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_credits", ["email"], name: "index_user_credits_on_email", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.boolean  "admin"
    t.boolean  "artist"
    t.boolean  "moderator"
    t.boolean  "supervisor"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "comments", "posts"
  add_foreign_key "favorites", "users"
  add_foreign_key "playlist_entries", "playlists"
  add_foreign_key "playlist_entries", "songs"
  add_foreign_key "playlists", "users"
end
