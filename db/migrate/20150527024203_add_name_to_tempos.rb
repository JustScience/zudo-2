class AddNameToTempos < ActiveRecord::Migration
  def change
    add_column :tempos, :name, :string
    add_column :tempos, :desc, :text
  end
end
