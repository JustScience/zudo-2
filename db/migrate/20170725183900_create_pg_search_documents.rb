class CreatePgSearchDocuments < ActiveRecord::Migration
  def self.up
    say_with_time("Creating table for pg_search multisearch") do
      create_table :pg_search_documents do |t|
        t.text :content
        t.integer :artist_id, :index => true
        t.integer  "genre_id", :index => true
        t.integer  "mood_id", :index => true
        t.integer  "pace_id", :index => true
        t.integer  "era_id", :index => true
        t.integer  "key_id", :index => true
        t.integer  "tempo_id", :index => true
        t.integer  "instrument_id", :index => true
        t.belongs_to :searchable, :polymorphic => true, :index => true
        t.timestamps null: false
      end
    end
  end

  def self.down
    say_with_time("Dropping table for pg_search multisearch") do
      drop_table :pg_search_documents
    end
  end
end
