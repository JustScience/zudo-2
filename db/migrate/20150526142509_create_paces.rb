class CreatePaces < ActiveRecord::Migration
  def change
    create_table :paces do |t|
      t.string :name
      t.text :desc

      t.timestamps null: false
    end
  end
end
