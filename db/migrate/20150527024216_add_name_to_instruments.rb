class AddNameToInstruments < ActiveRecord::Migration
  def change
    add_column :instruments, :name, :string
    add_column :instruments, :desc, :text
  end
end
