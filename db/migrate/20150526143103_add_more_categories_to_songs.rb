class AddMoreCategoriesToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :era_id, :integer
    add_column :songs, :key_id, :integer
  end
end
