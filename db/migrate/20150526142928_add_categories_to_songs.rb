class AddCategoriesToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :genre_id, :integer
    add_column :songs, :mood_id, :integer
    add_column :songs, :pace_id, :integer
  end
end
