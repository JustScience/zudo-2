class AddSimilarIdToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :similar_id, :integer
  end
end
