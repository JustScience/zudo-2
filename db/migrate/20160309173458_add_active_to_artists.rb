class AddActiveToArtists < ActiveRecord::Migration
  def up
  	add_column :artists, :active, :boolean, default: true
  end

  def down
  	remove_column :artists, :active
  end
end
