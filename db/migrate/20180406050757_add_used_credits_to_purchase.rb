class AddUsedCreditsToPurchase < ActiveRecord::Migration
  def change
  	add_column :purchases, :used_credits, :float, default: 0
  end
end
