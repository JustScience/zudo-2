class AddSampleToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :sample, :string
  end
end
