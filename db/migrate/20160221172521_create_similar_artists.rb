class CreateSimilarArtists < ActiveRecord::Migration
  def up
    create_table :similar_artists do |t|
    	t.integer :artist_id
    	t.integer :similar_artist_id
    	t.timestamps
    end
    add_index :similar_artists, :artist_id
    add_index :similar_artists, :similar_artist_id

    ##migrate old similar artists data
    Artist.find_each do |artist|
      SimilarArtist.create(artist_id: artist.id, similar_artist_id: artist.similar_id)
    end
  end

  def down
  	drop_table :similar_artists
  end
  
end