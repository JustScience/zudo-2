class CreatePurchaseItems < ActiveRecord::Migration
  def change
    create_table :purchase_items do |t|
      t.references :purchase, index: true
      t.references :song, index: true
      t.string :license
      t.integer :price_cents, default: 0, null: false
      t.string :price_currency, default: 'USD', null: false

      t.timestamps null: false
    end
  end
end
