class AddUserIdToProjectsSong < ActiveRecord::Migration
  def up
  	add_column :projects_songs, :user_id, :integer
  	add_index :projects_songs, :user_id
  end


  def down
  	remove_column :projects_songs, :user_id
  end
end
