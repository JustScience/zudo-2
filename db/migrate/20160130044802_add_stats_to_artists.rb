class AddStatsToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :hometown, :string
    add_column :artists, :pro, :string
    add_column :artists, :instrument1, :string
    add_column :artists, :instrument2, :string
    add_column :artists, :influence1, :string
    add_column :artists, :influence2, :string
    add_column :artists, :influence3, :string
  end
end
