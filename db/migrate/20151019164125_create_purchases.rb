class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :user, index: true
      t.timestamp :purchased_at

      t.timestamps null: false
    end
  end
end
