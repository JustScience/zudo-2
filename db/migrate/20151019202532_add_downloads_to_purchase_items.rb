class AddDownloadsToPurchaseItems < ActiveRecord::Migration
  def change
    add_column :purchase_items, :downloads, :integer, default: 0
  end
end
