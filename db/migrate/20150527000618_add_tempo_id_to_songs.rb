class AddTempoIdToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :tempo_id, :integer
  end
end
