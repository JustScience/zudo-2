class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :purchase, index: true
      t.string :token
      t.integer :amount_cents, default: 0, null: false

      t.timestamps null: false
    end
  end
end
