class AddFavoritesCountAndPlayCountToSongs < ActiveRecord::Migration
  def up
  	add_column :songs, :play_count, :integer, default: 0
  	add_column :songs, :favorites_count, :integer, default: 0

  	##migrate data for song favorite counts
  	Favorite.find_each do |fav|
  		song_id = fav.favorited_id
  		song = Song.find_by_id(song_id)
  		if song.present?
  			fav_count = song.favorites_count
  			song.update_column(:favorites_count, (fav_count+1))
  			puts "Song updated :: #{song.id}"
  		end
  	end

  end

  def down
  	remove_column :songs, :play_count
  	remove_column :songs, :favorites_count
  end

end
