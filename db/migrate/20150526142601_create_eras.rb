class CreateEras < ActiveRecord::Migration
  def change
    create_table :eras do |t|
      t.string :name
      t.text :desc

      t.timestamps null: false
    end
  end
end
