class CreateProjects < ActiveRecord::Migration
  def up
    create_table :projects do |t|
    	t.string :name
    	t.text :desc
    	t.integer :user_id
      t.timestamps
    end
    add_index :projects, :user_id

    create_table :projects_songs do |t|
    	t.integer :project_id
    	t.integer :song_id
    	t.text :desc
      t.timestamps
    end
    add_index :projects_songs, :project_id
    add_index :projects_songs, :song_id
  end

  def down
  	drop_table :projects_songs
  	drop_table :projects
  end

end
