class CreateUserCredits < ActiveRecord::Migration
  def up
    create_table :user_credits do |t|
    	t.string :email
    	t.float :total_credits_till_date, default: 0
    	t.float :used_credits, default: 0
    	t.float :remaining_credits, default: 0
    	t.timestamps
    end
    add_index :user_credits, :email
    add_column :shopping_carts, :used_credits, :float, default: 0
  end

  def down
  	drop_table :user_credits
  	remove_column :shopping_carts, :used_credits
  end
end
