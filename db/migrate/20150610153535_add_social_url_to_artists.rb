class AddSocialUrlToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :facebook, :string
    add_column :artists, :twitter, :string
    add_column :artists, :instagram, :string
    add_column :artists, :soundcloud, :string
    add_column :artists, :website, :string
    add_column :artists, :label, :string
    add_column :artists, :manager, :string
  end
end
