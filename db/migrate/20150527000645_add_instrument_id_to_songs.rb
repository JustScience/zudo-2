class AddInstrumentIdToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :instrument_id, :integer
  end
end
