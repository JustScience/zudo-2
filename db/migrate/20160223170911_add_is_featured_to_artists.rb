class AddIsFeaturedToArtists < ActiveRecord::Migration
  def up
  	add_column :artists, :is_featured, :boolean, default: false
  end

  def down
  	remove_column :artists, :is_featured
  end
  
end
