class CreateDeniedSongs < ActiveRecord::Migration
  def change
    create_table :denied_songs do |t|
    	t.string   "title"
      t.string   "desc"
	    t.integer  "artist_id"
	    t.integer  "genre_id"
	    t.integer  "mood_id"
	    t.integer  "pace_id"
	    t.integer  "era_id"
	    t.integer  "key_id"
	    t.integer  "tempo_id"
	    t.integer  "instrument_id"
	    t.integer  "user_id"
	    t.text "reason"
	    t.timestamps
    end
  end
end
