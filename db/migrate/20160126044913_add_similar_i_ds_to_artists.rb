class AddSimilarIDsToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :similar_ids, :integer, array:true, default: []
  end
end
