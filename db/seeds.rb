User.create!([
  {email: "j@zudo.me", password: "qwer1234", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2015-06-12 03:47:13", last_sign_in_at: "2015-06-12 03:47:13", current_sign_in_ip: "::1", last_sign_in_ip: "::1", name: "J Galenti", admin: true, artist: true, moderator: true}
])
Artist.create!([
  {name: "JustScience", bio: "JustScience is a multimedia artist and music producer born in Buffalo, NY trained in Chicago, IL and polished in the Bay Area, CA - J.Sci has a wide range of musical styles and artistic talents and now runs his production company in the mountains of Santa Fe. With his unique mix of hip-hop, dance, and pop JustScience is sure to move a crowd.\r\n\r\nAs a digital artist, J's skills range from photo manipulation and motion graphics to sound design and video editing. He has also excelled in interface & web design, special effects compositing, and videography. With J's wide range of interests and skills, he has dubbed the movement of \"the New Renaissance\" with a focus on his \"JustScience\" philosophy.", remote_image_url: 'https://s3-us-west-2.amazonaws.com/zudosongs/justscience-headshot.jpg', user_id: 2, facebook: "https://www.facebook.com/JSciBeats", twitter: "https://twitter.com/JustScience", instagram: "https://instagram.com/justscience", soundcloud: "https://soundcloud.com/jscibeats", website: "http://www.JSciBeats.com", label: "IN:DEEP Arts", manager: "Soozi Galenti"},
  # {name: "J Childs", bio: "J Childs is a composer and producer from St Louis, MO. His styles range from ambient and orchestral scores to hard-knocking east coast style hip hop and R&B beats. ManJay Productions exists to help artists with quality audio services to better enhance the musical experience for both artists and listeners.", remote_image_url: 'https://s3-us-west-2.amazonaws.com/zudosongs/j_childs-headshot.jpg', user_id: 3, facebook: "https://www.facebook.com/manjayproductions", twitter: "https://www.twitter.com/manjayprod", instagram: "", soundcloud: "https://soundcloud.com/manjay-productions", website: "http://www.manjayproductions.com", label: "ManJay Productions", manager: "J Childs"},
  # {name: "JDP", bio: "Johnathan “JDP” Pratt was born on the South Side of Chicago May 7, 1987. He grew up listening to a wide variety of music drawing influences from the likes of Frank Sinatra, Bob Dylan, LL Cool J, and Jay-Z. An avid do-it-yourselfer, JDP began writing, producing, and recording own his own music in high school, perfecting his sound and learning every aspect of the creative processes. \r\n\r\n In 2009, JDP released the mixtape “Air Raid”, sponsored by Los Angeles based clothing company IM King. The project featured Swedish hip hop sensation Adam Tensta, local favorites YP and Big Homie Doe, as well as professional skateboarder turned rapper, Terry “T.K.” Kennedy. The mixtapeʼs pop infused single “Release” led to JDPʼs inclusion in URB Magazines annual Next 100 issue alongside Drake, Asher Roth, Theophilus London and Freddie Gibbs. His second single, “Styrofoam Cups,” also garnered a solid of buzz after the video made its round on a number of popular blogs (HipHopDX and FakeshoreDrive), and the track was featured on the mix shows of Chicago’s #1 Hip Hop station, 107.5 WGCI.  \r\n\r\nFollowing “Air Raid”, JDP released 2010ʼs “High Times” (hosted by former Miss High Times Sara Newton) and 2011ʼs “Where Anything Goes Vol. 2” mixtapes showcasing both original tracks and industry freestyles. In fall 2011, JDP was featured in Coke Zero’s “Make It Possible” campaign, receiving the most hits for his songs “Lights Out (Girls To The Floor)” and “Circa ʻ87 feat. Adam Tensta”. Shortly after the campaigns viral success he was asked by Music Dealers to perform at their CMJ Showcase in New York City’s Thompson Hotel.  \r\n\r\nJDP's next major placement would come in the spring of 2012, when his unreleased record \"Beware The Wild Things\" was featured on an episode of MTV's Jersey Shore. Followed swiftly after came placements on MTV's \"The Real World\" & E's \"Keeping up with the Kardashians.” Most recently, JDP’s song “Where The Sidewalk Ends” was featured in the game “Watch Dogs” as an in game song on the soundtrack. There’s much to more to come for JDP, stay tuned.  ", remote_image_url: 'https://s3-us-west-2.amazonaws.com/zudosongs/jdp-headshot.jpg', user_id: nil, facebook: "https://www.facebook.com/thisisjdp", twitter: "https://twitter.com/ThisisJDP", instagram: "https://instagram.com/thisisjdp/", soundcloud: "https://soundcloud.com/thisisjdp", website: "http://www.thisisjdp.com", label: "Unsigned", manager: "Jonathan Pratt"}
])
Era.create!([
  {name: "20's", desc: "Songs reminiscent of the 1920's"},
  {name: "30's", desc: "Songs reminiscent of the 1930's"},
  {name: "40's", desc: "Songs reminiscent of the 1940's"},
  {name: "50's", desc: "Songs reminiscent of the 1950's"},
  {name: "60's", desc: "Songs reminiscent of the 1960's"},
  {name: "70's", desc: "Songs reminiscent of the 1970's"},
  {name: "80's", desc: "Songs reminiscent of the 1980's"},
  {name: "90's", desc: "Songs reminiscent of the 1990's"},
  {name: "Millenium", desc: "Songs reminiscent of the early 2000's"},
  {name: "Modern", desc: "Songs that sound like now"}
])
Genre.create!([
  {name: "Acoustic", desc: "Acoustic singer and songwriter style similar to bob dylan or jack johnson"},
  {name: "Jazz", desc: "Jazz is the first true american musical genre"},
  {name: "Hip Hop", desc: "Beats and Rhymes Make Hip Hop Music"},
  {name: "Rock", desc: "Rock and Roll Music with the Standard 4-Piece Back"},
  {name: "Classical", desc: "Classical and Cinematic Orchestral Music"},
  {name: "Electronic", desc: "Synth and Bass Driven Electronic Music with Hard Hitting Drums"}
])
Instrument.create!([
  {name: "Guitar", desc: "Songs featuring guitar"},
  {name: "Drums", desc: "Songs featuring drums"},
  {name: "Brass", desc: "Songs featuring horns"},
  {name: "Strings", desc: "Songs featuring orchestral strings"},
  {name: "Bass", desc: "Songs featuring bass"},
  {name: "Saxophone", desc: "Songs featuring saxophone"},
  {name: "Synth", desc: "Songs featuring synthesizers"},
  {name: "Voice", desc: "Songs featuring vocalists or choirs"},
  {name: "Percussion", desc: "Songs featuring percussion"},
  {name: "Ethnic", desc: "Songs featuring ethnic instruments"},
  {name: "Piano", desc: "Songs featuring piano"}
])
Key.create!([
  {name: "C", desc: "Songs in the key of C are Completely pure. Their character is: innocence, simplicity, naïvety, children's talk."},
  {name: "Cm", desc: "Songs in the key of Cm. Declaration of love and at the same time the lament of unhappy love. All languishing, longing, sighing of the love-sick soul lies in this key."},
  {name: "Db", desc: "Songs in the key of Db. A leering key, degenerating into grief and rapture. It cannot laugh, but it can smile; it cannot howl, but it can at least grimace its crying. Consequently only unusual characters and feelings can be brought out in this key."},
  {name: "D", desc: "Songs in the key of D. The key of triumph, of Hallejuahs, of war-cries, of victory-rejoicing. Thus, the inviting symphonies, the marches, holiday songs and heaven-rejoicing choruses are set in this key."},
  {name: "Dm", desc: "Songs in the key of Dm. Melancholy womanliness, the spleen and humours brood."},
  {name: "D#m", desc: "Songs in the key of D#m. Feelings of the anxiety of the soul's deepest distress, of brooding despair, of blackest depresssion, of the most gloomy condition of the soul. Every fear, every hesitation of the shuddering heart, breathes out of horrible D# minor. If ghosts could speak, their speech would approximate this key."},
  {name: "Eb", desc: "Songs in the key of Eb. The key of love, of devotion, of intimate conversation with God."},
  {name: "E", desc: "Songs in the key of E. Noisy shouts of joy, laughing pleasure and not yet complete, full delight lies in E Major."},
  {name: "F", desc: "Songs in the key of F. Complaisance and calm."},
  {name: "Fm", desc: "Songs in the key of Fm. Deep depression, funereal lament, groans of misery and longing for the grave."},
  {name: "F#", desc: "Songs in the key of F#. Triumph over difficulty, free sigh of relief utered when hurdles are surmounted; echo of a soul which has fiercely struggled and finally conquered lies in all uses of this key."},
  {name: "F#m", desc: "Songs in the key of F#m. A gloomy key: it tugs at passion as a dog biting a dress. Resentment and discontent are its language."},
  {name: "G", desc: "Songs in the key of G. Everything rustic, idyllic and lyrical, every calm and satisfied passion, every tender gratitude for true friendship and faithful love,--in a word every gentle and peaceful emotion of the heart is correctly expressed by this key."},
  {name: "Gm", desc: "Songs in the key of Gm. Discontent, uneasiness, worry about a failed scheme; bad-tempered gnashing of teeth; in a word: resentment and dislike."},
  {name: "Ab", desc: "Songs in the key of Ab. Key of the grave. Death, grave, putrefaction, judgment, eternity lie in its radius."},
  {name: "Abm", desc: "Songs in the key of Abm. Grumbler, heart squeezed until it suffocates; wailing lament, difficult struggle; in a word, the color of this key is everything struggling with difficulty."},
  {name: "A", desc: "Songs in the key of A. This key includes declarations of innocent love, satisfaction with one's state of affairs; hope of seeing one's beloved again when parting; youthful cheerfulness and trust in God."},
  {name: "Am", desc: "Songs in the key of Am. Pious womanliness and tenderness of character."},
  {name: "Bb", desc: "Songs in the key of Bb. Cheerful love, clear conscience, hope aspiration for a better world."},
  {name: "Bbm", desc: "Songs in the key of Bbm. A quaint creature, often dressed in the garment of night. It is somewhat surly and very seldom takes on a pleasant countenance. Mocking God and the world; discontented with itself and with everything; preparation for suicide sounds in this key."},
  {name: "B", desc: "Songs in the key of B. Strongly coloured, announcing wild passions, composed from the most glaring coulors. Anger, rage, jealousy, fury, despair and every burden of the heart lies in its sphere."},
  {name: "Bm", desc: "Songs in the key of Bm. This is as it were the key of patience, of calm awaiting ones's fate and of submission to divine dispensation."}
])
Mood.create!([
  {name: "Happy", desc: "Happy music is good music because it makes you happy when you're sad"},
  {name: "Sensual", desc: "Songs to get your blood pumping in a sensual way"},
  {name: "Angry", desc: "Angry songs full of rage"},
  {name: "Anthemic", desc: "Songs to get you stoked up"},
  {name: "Cinematic", desc: "Orchestral compositions with that big screen cinema feel"},
  {name: "Rebellious", desc: "Music of angst, change and system disruption"}
])
Pace.create!([
  {name: "Steady", desc: "Songs with a consistent level in energy, volume and intensity"},
  {name: "Rising", desc: "Songs with a constant increase in energy, volume and intensity"},
  {name: "Falling", desc: "Songs with a constant decrease in energy, volume and intensity"},
  {name: "Mid Dip", desc: "Songs with a distinct mid-point drop in energy, volume and intensity"},
  {name: "Mid Peak", desc: "Songs with a distinct mid-point peak in energy, volume and intensity"},
  {name: "Multi Peak", desc: "Songs with 2 or more distinct peaks in energy, volume and intensity"},
  {name: "Irratic", desc: "Songs with irratic, unstructured spikes and drops in energy, volume, and intensity"}
])
Tempo.create!([
  {name: "Under 60 bpm", desc: "Songs with a very slow speed"},
  {name: "60-80 bpm", desc: "Songs with a slow to semi slow speed"},
  {name: "80-100 bpm", desc: "Songs with a semi slow to moderate speed"},
  {name: "100-120 bpm", desc: "Songs with a moderate to semi fast speed"},
  {name: "120-140 bpm", desc: "Songs with a semi fast to fast speed"},
  {name: "Over 140 bpm", desc: "Songs with a very fast speed"}
])
