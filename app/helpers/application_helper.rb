module ApplicationHelper

	def get_similar_artist(similar_artist_id)
		Artist.where(id: similar_artist_id).first
	end

	def user_project_lists
		current_user.projects.pluck(:name, :id)
	end

end
