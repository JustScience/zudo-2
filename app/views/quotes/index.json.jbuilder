json.array!(@quotes) do |quote|
  json.extract! quote, :id, :title, :content, :image
  json.url quote_url(quote, format: :json)
end
