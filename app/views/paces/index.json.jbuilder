json.array!(@paces) do |pace|
  json.extract! pace, :id, :name, :desc
  json.url pace_url(pace, format: :json)
end
