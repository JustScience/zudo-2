json.array!(@moods) do |mood|
  json.extract! mood, :id, :name, :desc
  json.url mood_url(mood, format: :json)
end
