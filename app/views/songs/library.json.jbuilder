json.array!(@songs) do |song|
  json.extract! song, :id, :title, :desc, artist_id
  json.url song_url(song, format: :json)
end
