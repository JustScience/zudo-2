json.array!(@eras) do |era|
  json.extract! era, :id, :name, :desc
  json.url era_url(era, format: :json)
end
