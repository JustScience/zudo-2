json.array!(@tempos) do |tempo|
  json.extract! tempo, :id
  json.url tempo_url(tempo, format: :json)
end
