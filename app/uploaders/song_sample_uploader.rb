# encoding: utf-8
require 'cocaine'
class SongSampleUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :aws
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :watermark do
    process :mp3ize
    # process :watermark
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(mp3 wav wma ogg aif)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    if original_filename
      if model && model.read_attribute(mounted_as).present?
        model.read_attribute(mounted_as)
      else
        "#{secure_token}"
      end
    end
  end

  def set_duration
    meta = identify url(:watermark)
    model.update_attribute(:duration, meta[:duration])
  end

  protected

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end

  def watermark
    cache_stored_file! if !cached?
    inputs = [current_path, Rails.application.secrets.watermark_audio_url]
    input = inputs.map { |i| "-i \"#{i}\"" }.join(' ')
    input_params = '-filter_complex amix=inputs=2:duration=first'
    output = File.join(File.dirname(current_path), 'watermark.mp3')
    command = "avconv #{input} #{input_params} -y \"#{output}\""
    begin
      Cocaine::CommandLine.new(command, '', expected_outcodes: [0]).run
      File.rename output, current_path
      identify current_path
    rescue Cocaine::ExitStatusError => e
      puts "error while running command #{command}: #{e}"
    end
  end

  def mp3ize
    cache_stored_file! if !cached?
    output = File.join(File.dirname(current_path), 'mp3ized.mp3')
    command = "avconv -i \"#{current_path}\" -y \"#{output}\""

    meta = identify current_path
    if meta
      model.duration = meta[:duration]
    end

    begin
      Cocaine::CommandLine.new(command, '', expected_outcodes: [0]).run
      File.rename output, current_path
      meta
    rescue Cocaine::ExitStatusError => e
      puts "error while running command #{command}: #{e}"
    end
  end

  def identify(path)
    meta = {}
    command = "avconv -i \"#{path}\"  2>&1"
    out = Cocaine::CommandLine.new(command, '', expected_outcodes: [0, 1]).run.encode('UTF-8', 'UTF-8', invalid: :replace)
    out.split("\n").each do |line|
      if line =~ /(([\d\.]*)\s.?)fps,/
        meta[:fps] = $1.to_i
      end
      # Matching Stream #0.0: Audio: libspeex, 8000 Hz, mono, s16
      if line =~ /Audio:(.*)/
        meta[:audio_encode], meta[:audio_bitrate], meta[:audio_channels] = $1.to_s.split(',').map(&:strip)
      end
      # Matching Duration: 00:01:31.66, start: 0.000000, bitrate: 10404 kb/s
      if line =~ /Duration:(\s.?(\d*):(\d*):(\d*\.\d*))/
        meta[:length] = $2.to_s + ":" + $3.to_s + ":" + $4.to_s
        meta[:duration] = $2.to_i * 3600 + $3.to_i * 60 + $4.to_f
      end
    end
    if meta.empty?
      puts "Empty metadata from #{path}. Got the following output: #{out}"
    else
      return meta
    end
    nil
  end

end
