class ShoppingCartItemDecorator < ApplicationDecorator
  delegate_all

  def song_title
    object.song.title
  end

  def artist_name
    object.song.artist.name
  end

  def license
    License.find_by_slug(object.license).name
  end
end
