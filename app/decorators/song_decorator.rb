class SongDecorator < ApplicationDecorator
  delegate_all

  def facebook_share_link
    URI.encode "https://www.facebook.com/sharer/sharer.php?u=#{url}"
  end

  def twitter_share_link
    URI.encode "https://twitter.com/home?status=#{object.title} #{url}"
  end

  def google_plus_share_link
    URI.encode "https://plus.google.com/share?url=#{url}"
  end
  
  def title_with_artist
    "#{object.title} by #{object.artist.name}"
  end

  def url
    h.song_url(object)
  end

  def duration
    mins = (object.duration / 60).to_i
    secs = (object.duration % 60).to_i
    [mins.to_s.rjust(2, '0'), secs.to_s.rjust(2, '0')].reject(&:blank?).join ':'
  end

  def og_meta_tags
    {
        site_name: 'Zudo Music Licensing for Film, TV & Video Games',
        title: "License " + object.title + ' by ' + object.artist.name + ' for Film, TV and Video Games',
        type: 'music.song',
        audio: object.sample.url,
        url: url,
        image: object.artist.image.url,
        description: object.desc
    }
  end
end
