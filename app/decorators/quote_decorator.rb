class QuoteDecorator < ApplicationDecorator
  delegate_all

  def facebook_share_link
    URI.encode "https://www.facebook.com/sharer/sharer.php?u=#{url}"
  end

  def twitter_share_link
    URI.encode "https://twitter.com/home?status=#{object.title} #{url}"
  end

  def google_plus_share_link
    URI.encode "https://plus.google.com/share?url=#{url}"
  end

  def url
    h.quote_url(object)
  end

  def og_meta_tags
    {
        site_name: 'Zudo Music Licensing for Film, TV & Video Games',
        title: "Share " + object.title,
        url: url,
        image: object.image.url,
        description: object.content
    }
  end
end
