class PlaylistsController < ApplicationController
  before_action :set_playlist, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:show, :index, :create]

  def index
    @playlists = Playlist.all
  end

  def show
    @songs = Song.all.order('title ASC')
    @artists = Artist.all.order('name ASC')
  end

  def new
    @playlist = Playlist.new
  end

  def create
    @playlist = Playlist.new(playlist_params)
    respond_to do |format|
      if @playlist.save
        format.html { redirect_to @playlist, notice: 'Playlist was successfully created.' }
        format.json { render :show, status: :created, location: @playlist }
      else
        format.html { render :new }
        format.json { render json: @playlist.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @playlist = Playlist.find(params[:id])
  end

  def update
    respond_to do |format|
      if @playlist.update(playlist_params)
        format.html { redirect_to @playlist, notice: 'Playlist was successfully updated.' }
        format.json { render :show, status: :ok, location: @playlist }
      else
        format.html { render :edit }
        format.json { render json: @playlist.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @playlist = Playlist.find(params[:id])
    @playlist.destroy
    redirect_to playlists_url
  end

  def add_song
    @playlist = Playlist.find(params[:id])
    @playlist.playlist_entries.create(song_id: params[:song_id])
    redirect_to @playlist
  end

  def remove_song
    @playlist = Playlist.find(params[:id])
    entries = @playlist.playlist_entries.where(song_id: params[:song_id]).destroy_all
    redirect_to @playlist
  end

  private

  def set_playlist
    @playlist = Playlist.find(params[:id])
  end

  def playlist_params
    params.require(:playlist).permit(:name, :image)
  end
end