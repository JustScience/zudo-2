class PurchaseItemsController < ApplicationController

  load_and_authorize_resource only: [:placements]

  def index
    @purchase_items = PurchaseItem.joins(purchase: [:user]).where(users: {id: current_user.id}).order(created_at: :desc).decorate
  end

  def download
    purchase_item = PurchaseItem.find(params[:id])
    if purchase_item.downloads > 0
      data = open(purchase_item.song.sample_url)
      purchase_item.decrement!(:downloads)
      send_data data.read, filename: "#{purchase_item.song.title}.wav", type: 'audio/wav', x_sendfile: true
    else
      redirect_to purchases_path, alert: 'Download limit reached.'
    end
  end

  def placements
    if current_user.admin?
      @placements = PurchaseItem.order(created_at: :desc).decorate
    else
      @placements = PurchaseItem.joins(song: :artist).where(artists: {user_id: current_user.id}).order(created_at: :desc).decorate
    end
  end

end
