class PacesController < ApplicationController
  before_action :set_pace, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:index, :create, :show]
  # GET /paces
  # GET /paces.json
#  def index
#    @paces = Pace.all    
#  end

  # GET /paces/1
  # GET /paces/1.json
#  def show
#  end

  # GET /paces/new
  def new
  end

  # GET /paces/1/edit
  def edit
  end

  # POST /paces
  # POST /paces.json
  def create
    @pace = Pace.new(pace_params)
    respond_to do |format|
      if @pace.save
        format.html { redirect_to @pace, notice: 'Pace was successfully created.' }
        format.json { render :show, status: :created, location: @pace }
      else
        format.html { render :new }
        format.json { render json: @pace.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paces/1
  # PATCH/PUT /paces/1.json
  def update
    respond_to do |format|
      if @pace.update(pace_params)
        format.html { redirect_to @pace, notice: 'Pace was successfully updated.' }
        format.json { render :show, status: :ok, location: @pace }
      else
        format.html { render :edit }
        format.json { render json: @pace.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paces/1
  # DELETE /paces/1.json
  def destroy
    @pace.destroy
    respond_to do |format|
      format.html { redirect_to paces_url, notice: 'Pace was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pace
      @pace = Pace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pace_params
      params.require(:pace).permit(:name, :desc, :icon)
    end
end
