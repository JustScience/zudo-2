class ShoppingCartItemsController < ApplicationController
  before_action :set_shopping_cart
  before_action :set_shopping_cart_item, only: [:destroy]

  def index
    @shopping_cart_items = @shopping_cart.shopping_cart_items.decorate
    if @shopping_cart.total.present? && @shopping_cart.total.to_f < 0 && @shopping_cart.used_credits.present? && @shopping_cart.used_credits > 0
        new_credits = @shopping_cart.used_credits.to_f + @shopping_cart.total.to_f
        @shopping_cart.update(used_credits: new_credits)
    end
  end

  def checkout

     user_credit = UserCredit.where(email: current_user.email).first
     if user_credit.present? && user_credit.remaining_credits.present? && user_credit.remaining_credits <= 0 && @shopping_cart.present? && @shopping_cart.used_credits.present? && @shopping_cart.used_credits > 0
         @shopping_cart.update(used_credits: 0)
         return redirect_to cart_path, alert: "You have already used all of your credits.Your credits has been removed from your shopping cart."
     end

      if params[:stripeEmail].present? && params[:stripeToken].present?

          customer = Stripe::Customer.create(
                email: params[:stripeEmail],
                card: params[:stripeToken]
            )

          charge = Stripe::Charge.create(
                customer: customer.id,
                amount: @shopping_cart.total.cents,
                description: 'Purchase from Zudo',
                currency: 'usd'
          )

          purchase_items = @shopping_cart.shopping_cart_items.map { |cart_item| {song_id: cart_item.song_id, license: cart_item.license, price_cents: cart_item.price_cents, price_currency: cart_item.price_currency} }
          purchase =  current_user.purchases.create(purchase_items_attributes: purchase_items, payment_attributes: {token: charge.id, amount_cents: charge.amount}, used_credits:  @shopping_cart.used_credits)
      
      else
          purchase_items = @shopping_cart.shopping_cart_items.map { |cart_item| {song_id: cart_item.song_id, license: cart_item.license, price_cents: cart_item.price_cents, price_currency: cart_item.price_currency} }
          purchase =  current_user.purchases.create(purchase_items_attributes: purchase_items, payment_attributes: {token: SecureRandom.hex(4), amount_cents: (@shopping_cart.total * 100)}, used_credits:  @shopping_cart.used_credits)
      
      end  

    if purchase
      sc_used_credits = @shopping_cart.used_credits
      @shopping_cart.destroy
      session[:shopping_cart_id] = nil
      user_credit = UserCredit.where(email: current_user.email).first
      if user_credit.present?
          user_credit.used_credits += sc_used_credits
          user_credit.remaining_credits = user_credit.total_credits_till_date - user_credit.used_credits
          user_credit.save
      end
      user_email = (customer.present? && customer.sources.present?) ?  customer.sources.data.first.try(:name) : current_user.email
      UserMailer.purchase_user_details(params[:user_details]).deliver_now if params[:user_details].present?  
      UserMailer.payment_success(user_email, purchase.purchase_items.decorate).deliver_now if user_email.present?      
      redirect_to purchases_path, notice: 'Successfully purchased.'
    end
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to cart_path
  end

  def destroy
    @shopping_cart_item.destroy
    redirect_to cart_path
  end

  private

  def set_shopping_cart
    shopping_cart_id = session[:shopping_cart_id]
    @shopping_cart = shopping_cart_id ? ShoppingCart.find(shopping_cart_id) : ShoppingCart.create
    session[:shopping_cart_id] = @shopping_cart.id
  end

  def set_shopping_cart_item
    @shopping_cart_item = @shopping_cart.shopping_cart_items.find(params[:id])
  end
end
