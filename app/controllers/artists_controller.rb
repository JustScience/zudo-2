class ArtistsController < ApplicationController
  before_action :set_artist, only: [:show, :edit, :update, :destroy, :save_similar_artist, :save_featured, :save_as_active]
  load_and_authorize_resource except: [:create, :show, :new_artist_submission]

  # GET /artists
  # GET /artists.json
  def index
    @artists = Artist.active.order('name ASC')
  end

  # GET /artists/1
  # GET /artists/1.json
  def show
    if params[:q].present? && params[:q].squish.present?
      query = params[:q].squish
      result = PgSearch.multisearch(query).includes(:searchable).where(artist_id: @artist.id)
      paginate_songs = result.map(&:searchable).sort_by! { |song| song.title }
      @songs = PaginatingDecorator.decorate(Kaminari.paginate_array(paginate_songs).page(params[:page]).per(20))
    else
      @songs = @artist.songs.order('title ASC').page(params[:page]).decorate
    end
    @similar_artist_ids = @artist.similar_artists.map(&:similar_artist_id)
  end

  def relationships
    @artists = Artist.all.order('name ASC')
    @active_artists = Artist.active.order('name ASC')
  end
  
  ##save similar artist for artist
  def save_similar_artist
    if params[:similar_artist].present? && params[:similar_artist][:similar_ids].present?
      ids = params[:similar_artist][:similar_ids]

      ##delete old similar artists and build new
      @artist.similar_artists.delete_all
      ids.collect do |id| 
        SimilarArtist.create(artist_id: @artist.id, similar_artist_id: id) if id.present?
      end
    end
  end
  
  def save_featured
    @artist.update_column(:is_featured, params[:data]) if params[:data].present?  
    render nothing: true
  end

  def save_as_active
    @artist.update_column(:active, params[:data]) if params[:data].present?  
    render nothing: true
  end

  # GET /artists/new
  def new
  end

  # GET /artists/1/edit
  def edit
  end

  # POST /artists
  # POST /artists.json
  def create
    @artist = Artist.new(artist_params)
    respond_to do |format|
      if @artist.save
        format.html { redirect_to @artist, notice: 'Artist was successfully created.' }
        format.json { render :show, status: :created, location: @artist }
      else
        format.html { render :new }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /artists/1
  # PATCH/PUT /artists/1.json
  def update
    respond_to do |format|
      if @artist.update(artist_params)
        format.html { redirect_to @artist, notice: 'Artist was successfully updated.' }
        format.json { render :show, status: :ok, location: @artist }
      else
        format.html { render :edit }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /artists/1
  # DELETE /artists/1.json
  def destroy
    @artist.destroy
    respond_to do |format|
      format.html { redirect_to artists_url, notice: 'Artist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  

  ##submit new musics / artists
  def new_artist_submission
    if request.post?
      return redirect_to submit_your_music_path, notice: "Please fill all details." if params[:music].blank?
      UserMailer.submit_music(params[:music]).deliver_now
      redirect_to root_path, notice: "Thank you for your interest. We'll get back to you soon."
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_artist
      @artist = Artist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def artist_params
      params.require(:artist).permit( :user_id,
                                      :name, 
                                      :bio, 
                                      :image, 
                                      :hometown, 
                                      :label, 
                                      :manager, 
                                      :pro, 
                                      :website, 
                                      :facebook, 
                                      :twitter, 
                                      :instagram, 
                                      :soundcloud, 
                                      :instrument1, 
                                      :instrument2, 
                                      :influence1, 
                                      :influence2, 
                                      :influence3, 
                                      :similar_id, 
                                      :similar_ids
                                    )
    end
end
