class ProjectsController < ApplicationController

  respond_to :html, :json
  before_action :authenticate_user!, except: [:show]
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :validate_user_project, only: [:edit, :update, :destroy]
  before_action :set_song, only: [:add_song, :submit_song]
  

  def index
    @projects = current_user.projects.order("created_at desc")
  end

  def show
    @songlist = ProjectsSong.where(project_id: @project.id).page(params[:page]).order("created_at desc")
  end

  # GET /paces/new
  def new
    return redirect_to projects_path, notice: "You can create only 5 projects." if current_user.projects.size >= 5
    @project = Project.new
  end

  # GET /paces/1/edit
  def edit
  end

  # POST /paces
  # POST /paces.json
  def create
    @project = Project.new(project_params)
    @project.user_id = current_user.id

    return redirect_to projects_path, notice: "You can create only 5 projects." if current_user.projects.size >= 5
    if @project.save
      redirect_to projects_path, notice: 'Project was successfully created.' 
    else
      flash[:error] = 'Project was not created.' 
      render action: :new
    end
  end

  # PATCH/PUT /paces/1
  # PATCH/PUT /paces/1.json
  def update
    if @project.update(project_params)
      redirect_to projects_path, notice: 'Project was successfully updated.' 
    else
      flash[:error] = 'Project was not updated.' 
      render action: :edit
    end
  end

  # DELETE /paces/1
  # DELETE /paces/1.json
  def destroy
    @project.destroy if @project.present?
    redirect_to projects_path, notice: 'Project was successfully destroyed.'   
  end

  def add_song
    respond_modal_with @song
  end

  def edit_song
    @project_song = ProjectsSong.find_by_id(params[:id])
    return redirect_to root_path, notice: "Wrong Access." if @project_song.blank? || (@project_song.user_id != current_user.id)
    @song = @project_song.song
    respond_modal_with @song
  end

  def delete_song
    project_song = ProjectsSong.find_by_id(params[:id])
    return redirect_to root_path, notice: "Wrong Access." if project_song.blank? || (project_song.user_id != current_user.id)
    redirect_to request.referer, notice: 'Song was successfully deleted from your project.' if project_song && project_song.destroy
  end

  def submit_song
    @errors  = {}
    @errors[:project] = "Project can't be blank" if params[:project_song][:project_id].blank?
    return render :submit_song if @errors.present?
    #@errors[:desc] = "Notes can't be blank" if params[:project_song][:desc].blank?
    #return render :submit_song if @errors.present?

    project_id = params[:project_song][:project_id]
    song_id = params[:song_id]
    project_name = Project.where(id: project_id).first.try(:name)
    
    project_song = ProjectsSong.where(user_id: current_user.id, project_id: project_id, song_id: song_id).first
    
    if project_song.blank?
      project_song = ProjectsSong.new(project_song_params) 
      project_song.song_id = @song.try(:id)
      project_song.user_id = current_user.id
      project_song.save
      @success = "Song successfully added in your Project #{project_name}."
    else
      project_song.update(project_song_params)
       @success = "Song successfully updated in your Project #{project_name}."
    end

  end

  def submit_project
    @errors  = {}
    @errors = "Project name can't be blank" if params[:project][:name].blank?
    return render :submit_project if @errors.present?
   
    @project = Project.new(project_params)
    @project.user_id = current_user.id

    @errors =  "You can create only 5 projects." if current_user.projects.size >= 5
    return render :submit_project if @errors.present?

    if @project.save

      @success = "Project was successfully created. Please select from the list."
      @projects = current_user.projects.order("created_at desc").pluck(:name, :id)
      
    else
      @errors = @project.errors.full_messages[0]
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find_by_id(params[:id])
      return redirect_to root_path, notice: "Project does not exists." if @project.blank?
    end

    def validate_user_project
      return redirect_to root_path, notice: "Wrong Access." if @project.user_id != current_user.id
    end

    def set_song
      @song = Song.find(params[:song_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :desc)
    end

    def project_song_params
      params.require(:project_song).permit(:project_id, :desc)
    end
end
