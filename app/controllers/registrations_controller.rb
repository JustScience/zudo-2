class RegistrationsController < Devise::RegistrationsController

  protected

  def after_sign_up_path_for(resource)
    play_path(after_sign_up: true)
  end
end