class PostsController < ApplicationController
	before_action :set_post, only: [:show, :edit, :update, :destroy]
  	load_and_authorize_resource except: [:index, :create, :show]
	def index
		@posts = Post.all.order('created_at DESC')
	end

	def show
	end

	def new
	end

	def create
		@post = Post.new(post_params)
		
		if @post.save
			redirect_to @post
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		
		if @post.update(params[:id]).permit(:image,:title,:body,:author,:keyword,:desc)
			redirect_to @post
		else
			render 'edit'
		end
	end

	def destroy
		@post.destroy

		redirect_to posts_path
	end

	private
		def set_post
			@post = Post.find(params[:id])
		end
		def post_params
			params.require(:post).permit(:image,:title,:body,:author,:keyword,:desc)
		end
end
