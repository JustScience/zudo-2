class FavoriteProjectsController < ApplicationController
  before_action :set_song

  def create
    if Favorite.create(favorited: @song, user: current_user)
      render json: {message: 'Song has been favorited', song_id: @song.id}, status: :ok
    else
      render json: {message: 'Something went wrong...*sad panda*', song_id: @song.id}, status: :unprocessable_entity
    end
  end
  
  def destroy
    Favorite.where(favorited_id: @song.id, user_id: current_user.id).first.destroy
    render json: {message: 'Song is no longer in favorites', song_id: @song.id}, status: :ok
  end
  
  private
  
  def set_song
    @song = Song.find(params[:song_id] || params[:id])
  end
end