class SongsController < ApplicationController
  respond_to :html, :json

  before_action :authenticate_user!, only: [:seller, :new, :create, :edit, :update, :destroy]
  before_action :set_song, only: [:show, :show, :edit, :download, :share, :update, :destroy, :add_to_cart, :update_play_count]
  before_action :set_filters, only: [:library]
  before_action :set_shopping_cart, only: [:add_to_cart]
  load_and_authorize_resource except: [:library, :favorite, :add_to_cart, :create, :show, :share, :download]

  #Download 
  def download
    data = open(@song.sample_url(:watermark))
    send_data data.read, filename: "#{@song.title}.mp3", type: 'audio/mp3', x_sendfile: true
  end

  def share
    respond_modal_with @song
  end

  def add_to_cart
    if request.xhr?
      respond_modal_with @song
    else
      license = License.find_by_slug(params[:license])
      @shopping_cart.add(@song, license.price, license.slug)
      redirect_to cart_path, notice: 'The song is successfully added to cart.'
    end
  end

  # GET /songs
  # GET /songs.json
  def library
    if params[:q].present? && params[:q].squish.present?
      query = params[:q].squish
      result = PgSearch.multisearch(query).includes(:searchable).where(@filters)
      paginate_songs = result.map(&:searchable).sort_by! { |song| song.title }
      @songs = PaginatingDecorator.decorate(Kaminari.paginate_array(paginate_songs).page(params[:page]).per(20))
   
    else
      @songs = unless @filters.blank?
                 Song.where(@filters).page(params[:page]).order("random()")
               else
                 Song.page(params[:page]).order("random()")
               end
      @songs = @songs.decorate
    end

    if request.headers['X-PJAX']
      render 'songlist', layout: false and return
    elsif request.xhr?
      render 'library', layout: false and return    
    end

    @genres = Genre.all
    @moods = Mood.all
    @paces = Pace.all
    @eras = Era.all
    @instruments = Instrument.all
    @tempos = Tempo.all
    @keys = Key.all

    @after_sign_up = params[:after_sign_up]
  end

  def admin
    @songs = Song.all.order('title ASC')
    @artists = Artist.all.order('name ASC').map { |a| [a.name, a.id] }
    @genres = Genre.all.order('name ASC').map { |g| [g.name, g.id] }
    @moods = Mood.all.order('name ASC').map { |m| [m.name, m.id] }
    @paces = Pace.all.order('name ASC').map { |p| [p.name, p.id] }
    @eras = Era.all.order('name ASC').map { |e| [e.name, e.id] }
    @instruments = Instrument.all.order('name ASC').map { |i| [i.name, i.id] }
    @tempos = Tempo.all.order('name ASC').map { |t| [t.name, t.id] }
    @keys = Key.all.order('name ASC').map { |k| [k.name, k.id] }
  end

  def approvals
    @songs = Song.where(approved: false).order('created_at ASC').page(params[:page]).per(120)
    @unapproved_songs_count = Song.where(approved: false).count
  end

  def approve_deny
    if params[:status] == "a" ##song approval request
     @song.update(approved: true)
     @id = @song.id
     @msg = "Song successfully approved."
    elsif params[:status] == "d" ##song deny request
      @id = @song.id
      song = @song
      if @song.destroy
        attributes = {title: song.title, desc: song.desc, artist_id: song.artist_id,
        genre_id: song.genre_id, mood_id: song.mood_id, pace_id: song.pace_id, instrument_id: song.instrument_id,
        era_id: song.era_id, key_id: song.key_id, tempo_id: song.tempo_id, user_id: song.user_id, reason: params[:reason] }
        DeniedSong.create(attributes)
        @msg = "Song deleted from database and stored in Denied songs table."
      end
    end
  end

  def deny_song
    respond_modal_with @song
  end

  def denied_songs
    @denied_songs = DeniedSong.includes(:artist).order('created_at DESC')
  end

  def seller
    @songs = current_user.songs.page(params[:page])
  end

  def favorite
    @songs = current_user.favorite_projects.page(params[:page]).decorate
  end

  def add_song
    @playlist = Playlist.find(params[:id])
    @playlist.playlist_entries.create(song_id: params[:song_id])
  end

  def remove_song
    @playlist = Playlist.find(params[:id])
    entries = @playlist.playlist_entries.where(song_id: params[:song_id]).destroy_all
  end

  def update_play_count
    if @song.present?
      count = @song.play_count > 0 ? (@song.play_count + 1) : 1
      @song.update(play_count: count)
      render nothing: true
    end
  end

  # GET /songs/1
  # GET /songs/1.json
  def show
    @songs = @song.artist.songs.page(params[:page]).decorate
    @artist = @song.artist(params[:id])
    @similar_artist_ids = @artist.similar_artists.map(&:similar_artist_id)
  end

  # GET /songs/new
  def new
    @artists = Artist.all.order('name ASC').map { |a| [a.name, a.id] }
    @genres = Genre.all.order('name ASC').map { |g| [g.name, g.id] }
    @moods = Mood.all.order('name ASC').map { |m| [m.name, m.id] }
    @paces = Pace.all.order('name ASC').map { |p| [p.name, p.id] }
    @eras = Era.all.order('name ASC').map { |e| [e.name, e.id] }
    @instruments = Instrument.all.order('name ASC').map { |i| [i.name, i.id] }
    @tempos = Tempo.all.map { |t| [t.name, t.id] }
    @keys = Key.all.map { |k| [k.name, k.id] }

  end

  # GET /songs/1/edit
  def edit
    @artists = Artist.all.order('name ASC').order('name ASC').map { |a| [a.name, a.id] }
    @genres = Genre.all.order('name ASC').map { |g| [g.name, g.id] }
    @moods = Mood.all.order('name ASC').map { |m| [m.name, m.id] }
    @paces = Pace.all.order('name ASC').map { |p| [p.name, p.id] }
    @eras = Era.all.order('name ASC').map { |e| [e.name, e.id] }
    @instruments = Instrument.all.order('name ASC').map { |i| [i.name, i.id] }
    @tempos = Tempo.all.map { |t| [t.name, t.id] }
    @keys = Key.all.map { |k| [k.name, k.id] }
  end

  # POST /songs
  # POST /songs.json
  def create
    @song = current_user.songs.new(song_params)

    respond_to do |format|
      if @song.save
        format.html { redirect_to @song, notice: 'Song was successfully created.' }
        format.json { render :show, status: :created, location: @song }
      else
        format.html { redirect_to :back, notice: 'All fields are required to submit song!' }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /songs/1
  # PATCH/PUT /songs/1.json
  def update
    respond_to do |format|
      if @song.update(song_params)
        format.html { redirect_to @song, notice: 'Song was successfully updated.' }
        format.json { render :show, status: :ok, location: @song }
      else
        format.html { redirect_to :back, notice: 'All fields are required to update song!' }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /songs/1
  # DELETE /songs/1.json
  def destroy
    @song.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Song was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_song
    @song = Song.find(params[:id]).decorate
  end
  def set_playlist
    @song = Playlist.find(params[:id])
  end

  def set_shopping_cart
    shopping_cart_id = session[:shopping_cart_id]
    @shopping_cart = shopping_cart_id ? ShoppingCart.find(shopping_cart_id) : ShoppingCart.create
    session[:shopping_cart_id] = @shopping_cart.id
    
    user_credit = UserCredit.where(email: current_user.email).first
    if user_credit.present? && user_credit.remaining_credits.present? && user_credit.remaining_credits > 0
      @shopping_cart.update(used_credits: user_credit.remaining_credits)
    end
  end

  def set_filters
    unless params[:filters].blank?
      @filters = {}
      params[:filters].each do |category, option_ids|
        if %w(genre mood pace era key tempo instrument).include?(category)
          @filters["#{category}_id"] = option_ids
        end
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def song_params
    params.require(:song).permit(:title, :desc, :image, :sample, :artist_id, :genre_id, :mood_id, :pace_id, :era_id, :instrument_id, :tempo_id, :key_id, :approved)
  end
end
