class PagesController < ApplicationController
  def home
    @playlists = Playlist.all
    @artists = Artist.all
  end

  def about
    @playlists = Playlist.all
  end

  def film
    @playlists = Playlist.all
  end

  def games
    @playlists = Playlist.all
  end

  def video
    @playlists = Playlist.all
  end

  def horror
    @playlists = Playlist.all
    @artists = Artist.all
  end

  def ads
    @playlists = Playlist.all
  end

  def terms
  end

  def faq
  end

  def deny
  end

  def admin
    authorize! :manage, :all
    @songs = Song.all
    @approved = Song.where(approved: true)
    @queued = Song.where(approved: false)
    @artists = Artist.active.all
  end

  def romance
    @playlists = Playlist.all
    @artists = Artist.all
  end

end
