class SiteController < ApplicationController
  respond_to :html, :json

  def share_zudo
    @share_message = "I just found an awesome new Production Music Library for #Filmmaking & #GameDev | Check out www.ZudoMusic.com | #IAMZUDO @ZudoMusic"
    @facebook_share_link = URI.encode("https://www.facebook.com/sharer/sharer.php?u=http://www.ZudoMusic.com")
    @twitter_share_link = URI.encode("https://twitter.com/home?status=#{@share_message}")
    @google_plus_share_link = URI.encode("https://plus.google.com/share?url=http://www.ZudoMusic.com")
    respond_modal_with @share_message
  end

  def must_login
    respond_modal_with []
  end
end
