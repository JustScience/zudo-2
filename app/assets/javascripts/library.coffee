$ ->
  $(document).pjax('[data-pjax]', '[data-pjax-container]')
  $(document).on 'ready pjax:success', ->
    if $('#libraryPage').length > 0
      LibraryPage.ready()
    return
  return
  console.log(searchQuery)
LibraryPage = LibraryPage or
  ready: ->
    $('#song-filter-menu').mmenu
      extensions: [
        'theme-dark'
        'effect-menu-slide'
      ]
      counters: true
      dividers: fixed: true
      navbar:
        title: 'FILTERS'

    $('#song-filter-menu .option[type="checkbox"]').change (e) ->
      option = $(this)
      category = option.data('category')
      optionId = option.data('id')
      optionValue = option.data('value')

      if option.is(':checked')
        LibraryPage.addFilter(category, optionId, optionValue)
      else
        LibraryPage.removeFilterById(category, optionId)
      return

    $('#tags').tagsInput
      interactive: false

      onChange: (elem, tag) ->
        if tag?
          parts = tag.split(' - ')
          if parts.length is 2
            category = parts[0].slice(1, -1)
            $('.tag span:contains("' + tag + '")').closest('.tag').addClass(category)

        return

      onRemoveTag: (tag) ->
        if tag?
          parts = tag.split(' - ')
          if parts.length is 2
            category = parts[0].slice(1, -1)
            optionValue = parts[1]
            LibraryPage.removeFilterByValue(category, optionValue)

        return

    return

  activeFilters: {}

  tagTemplateString: '[<%= category %>] - <%= option %>'

  filterParams: ->
    params = filters: {}
    _.each(LibraryPage.activeFilters, (options, category) ->
      optionIds = _.map(options, (option) ->
        option.id
      )
      params.filters[category] = optionIds
    )
    params

  addFilter: (category, optionId, optionValue) ->
    options = LibraryPage.activeFilters[category] or []
    option = _.findWhere(options, id: optionId)

    unless option?
      options.push(id: optionId, value: optionValue)
      LibraryPage.activeFilters[category] = options

      tagTemplate = _.template(LibraryPage.tagTemplateString)
      $('#tags').addTag(tagTemplate(category: category, option: optionValue))

#      $('#song-filter-menu .option[type="checkbox"][data-category="' + category + '"][data-id="' + optionId + '"]').prop('checked', true)

      LibraryPage.filter()

    return

  removeFilterById: (category, optionId) ->
    options = LibraryPage.activeFilters[category] or []
    option = _.findWhere(options, id: optionId)

    if option?
      LibraryPage.activeFilters[category] = _.without(options, _.findWhere(options, id: optionId))
      tagTemplate = _.template(LibraryPage.tagTemplateString)

      $('#tags').removeTag(tagTemplate(category: category, option: option.value))

      $('#song-filter-menu .option[type="checkbox"][data-category="' + category + '"][data-id="' + option.id + '"]').prop('checked', false)

      LibraryPage.filter()

    return

  removeFilterByValue: (category, optionValue) ->
    options = LibraryPage.activeFilters[category] or []
    option = _.findWhere(options, value: optionValue)

    if option?
      LibraryPage.activeFilters[category] = _.without(options, _.findWhere(options, value: optionValue))
      tagTemplate = _.template(LibraryPage.tagTemplateString)

      $('#tags').removeTag(tagTemplate(category: category, option: option.value))

      $('#song-filter-menu .option[type="checkbox"][data-category="' + category + '"][data-id="' + option.id + '"]').prop('checked', false)

      LibraryPage.filter()

    return

  filter: () ->
    searchQuery = $('#search_query').val()
    if searchQuery
      $.pjax({url: '/play?q='+searchQuery, container: '[data-pjax-container]', data: LibraryPage.filterParams(), timeout: 5000})
      return
    else
      $.pjax({url: '/play', container: '[data-pjax-container]', data: LibraryPage.filterParams(), timeout: 5000})
      return