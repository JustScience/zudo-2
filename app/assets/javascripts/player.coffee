@ZudoPlayer = @ZudoPlayer ||
  jPlayerElement: '#zudo-player'
  jPlayerContainer: '#zudo-player-container'
  song: {}

  initialize: ->
    pp = Snap('#zudo-player-container svg.pp')
    bg = pp.path('M0,0L80,0L80,80L0,80z').attr(fill: '#fffffd')
    p1 = pp.path('M50,30L50,30L13,30L13,10z').attr(fill: '#4a707c')
    p2 = pp.path('M50,30L50,30L13,50L13,30z').attr(fill: '#4a707c')

    $(ZudoPlayer.jPlayerElement).jPlayer
      ready: (event) ->
        $(ZudoPlayer.jPlayerContainer).find('.jp-controls, .jp-progress, .jp-info, .jp-actions').hide()
        return
      play: ->
        p1.animate {d: 'M48,50L36,50L36,10L48,10z', fill: '#fffffd'}, 600, mina.easeinout
        p2.animate {d: 'M22,50L10,50L10,10L22,10z', fill: '#fffffd'}, 600, mina.easeinout
        bg.animate {fill: '#4a707c'}, 600, mina.easeinout
        $(ZudoPlayer.jPlayerContainer).find('.jp-controls, .jp-progress, .jp-info, .jp-actions').slideDown()
        return
      pause: ->
        p1.animate {d: 'M50,30L50,30L13,30L13,10z', fill: '#4a707c'}, 600, mina.easeinout
        p2.animate {d: 'M50,30L50,30L13,50L13,30z', fill: '#4a707c'}, 600, mina.easeinout
        bg.animate {fill: '#fffffd'}, 600, mina.easeinout
        $(ZudoPlayer.jPlayerContainer).find('.jp-controls, .jp-progress, .jp-info, .jp-actions').slideUp()
        return
      ended: ->
        if ZudoPlayer.song.next_id?
          ZudoPlayer.play(ZudoPlayer.song.playlist, ZudoPlayer.song.next_id)
        return
      cssSelectorAncestor: ZudoPlayer.jPlayerContainer
      swfPath: 'assets/swf'
      supplied: 'mp3'
      wmode: 'window'
      smoothPlayBar: true
      keyEnabled: true
      remainingDuration: true
      toggleDuration: true

    $(ZudoPlayer.jPlayerContainer).on 'click', '.jp-play-pause', ->
      if $('#zudo-player').data().jPlayer.status.paused
        if ZudoPlayer.songId?
          $(ZudoPlayer.jPlayerElement).jPlayer('play')
        else
          songElement = _.sample($('[data-playlist="zudo"]'))
          ZudoPlayer.play('zudo', $(songElement).data('song-id'))
      else
        $(ZudoPlayer.jPlayerElement).jPlayer('pause')

      return

    $(ZudoPlayer.jPlayerContainer).on 'click', '.jp-next', ->
      ZudoPlayer.next()
      return

    return

  play: (playlist, songId) ->
    if playlist? && songId?
      songData = $('[data-playlist="' + playlist + '"][data-song-id="' + songId + '"]').data('song')
      if songData?
        ZudoPlayer.song = songData
        ZudoPlayer.playlist = playlist
        ZudoPlayer.songId = songId
        $(ZudoPlayer.jPlayerContainer).find('.title').html(songData.title)
        $(ZudoPlayer.jPlayerContainer).find('.artist').html(songData.artist)
        $(ZudoPlayer.jPlayerContainer).find('.share').prop('href', songData.share_path)
        if songData.favorited is true
          $(ZudoPlayer.jPlayerContainer).find('.favorite').prop('href', songData.favorite_path).hide()
          $(ZudoPlayer.jPlayerContainer).find('.unfavorite').prop('href', songData.unfavorite_path).show()
        else
          $(ZudoPlayer.jPlayerContainer).find('.unfavorite').prop('href', songData.unfavorite_path).hide()
          $(ZudoPlayer.jPlayerContainer).find('.favorite').prop('href', songData.favorite_path).show()

        $(ZudoPlayer.jPlayerContainer).find('.add-to-cart').prop('href', songData.add_to_cart_path)
        $(ZudoPlayer.jPlayerElement).jPlayer('setMedia', {mp3: songData.url})
        $(ZudoPlayer.jPlayerElement).jPlayer('play')


    return

  next: ->
    if ZudoPlayer.songId? && ZudoPlayer.song.next_id?
      ZudoPlayer.play(ZudoPlayer.playlist, ZudoPlayer.song.next_id)
    return

$(document).ready ->
  if $('.songlist').length > 0 || $('.songShow').length > 0
    $('#zudo-player-container').show()
    ZudoPlayer.initialize()
    $('#zudo-player-container')
    .on 'ajax:beforeSend', '.favorite', (e, data, status, xhr) ->
      $(this).closest('span.favoriteProject').attr 'data-status', 'loading'
      return
    .on 'ajax:success', '.favorite', (e, data, status, xhr) ->
      song = $('[data-playlist="' + ZudoPlayer.playlist + '"][data-song-id="' + ZudoPlayer.songId + '"]')
      if song?
        song.closest('.songRow').find('span.favoriteProject').attr 'data-status', 'unfavorite'
        songData = song.data('song')
        songData.favorited = true
        song.data('song', songData)

      $('#zudo-player-container .favorite').hide()
      $('#zudo-player-container .unfavorite').show()
      $.gritter.add
        image: '/assets/success.png'
        title: 'Success'
        text: data.message
      return
    .on 'ajax:error', '.favorite', (e, data, status, xhr) ->
      song = $('[data-playlist="' + ZudoPlayer.playlist + '"][data-song-id="' + ZudoPlayer.songId + '"]')
      if song?
        song.closest('.songRow').find('span.favoriteProject').attr 'data-status', 'favorite'
      $('#zudo-player-container .favorite').show()
      $('#zudo-player-container .unfavorite').hide()
      $.gritter.add
        image: '/assets/error.png'
        title: 'Error'
        text: data.message
      return
    .on 'ajax:beforeSend', '.unfavorite', (e, data, status, xhr) ->
      $(this).closest('span.favoriteProject').attr 'data-status', 'loading'
      return
    .on 'ajax:success', '.unfavorite', (e, data, status, xhr) ->
      song = $('[data-playlist="' + ZudoPlayer.playlist + '"][data-song-id="' + ZudoPlayer.songId + '"]')
      if song?
        song.closest('.songRow').find('span.favoriteProject').attr 'data-status', 'favorite'
        songData = song.data('song')
        songData.favorited = false
        song.data('song', songData)
      $('#zudo-player-container .favorite').show()
      $('#zudo-player-container .unfavorite').hide()

      if $('#favorites-page').length > 0
        $(this).closest('.songRow').remove()

      $.gritter.add
        image: '/assets/success.png'
        title: 'Success'
        text: data.message
      return
    .on 'ajax:error', '.unfavorite', (e, data, status, xhr) ->
      song = $('[data-playlist="' + ZudoPlayer.playlist + '"][data-song-id="' + ZudoPlayer.songId + '"]')
      if song?
        song.closest('.songRow').find('span.favoriteProject').attr 'data-status', 'unfavorite'
      $('#zudo-player-container .favorite').hide()
      $('#zudo-player-container .unfavorite').show()
      $.gritter.add
        image: '/assets/error.png'
        title: 'Error'
        text: data.message
      return
  else
    $('#zudo-player-container').hide()
  return