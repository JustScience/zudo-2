$ ->
  $('.songlist')
  .on 'ajax:beforeSend', '.favorite', (e, data, status, xhr) ->
    $(this).closest('span.favoriteProject').attr 'data-status', 'loading'
    return
  .on 'ajax:success', '.favorite', (e, data, status, xhr) ->
    $(this).closest('span.favoriteProject').attr 'data-status', 'unfavorite'
    song = $(this).closest('.songRow').find('.songRowPlay')
    songData = song.data('song')
    songData.favorited = true
    song.data('song', songData)
    if "#{data.song_id}" is ZudoPlayer.songId
      $('#zudo-player-container .favorite').hide()
      $('#zudo-player-container .unfavorite').show()
    $.gritter.add
      image: '/assets/success.png'
      title: 'Success'
      text: data.message
    return
  .on 'ajax:error', '.favorite', (e, data, status, xhr) ->
    $(this).closest('span.favoriteProject').attr 'data-status', 'favorite'
    $.gritter.add
      image: '/assets/error.png'
      title: 'Error'
      text: data.message
    return
  .on 'ajax:beforeSend', '.unfavorite', (e, data, status, xhr) ->
    $(this).closest('span.favoriteProject').attr 'data-status', 'loading'
    return
  .on 'ajax:success', '.unfavorite', (e, data, status, xhr) ->
    $(this).closest('span.favoriteProject').attr 'data-status', 'favorite'
    song = $(this).closest('.songRow').find('.songRowPlay')
    songData = song.data('song')
    songData.favorited = false
    song.data('song', songData)
    if "#{data.song_id}" is ZudoPlayer.songId
      $('#zudo-player-container .favorite').show()
      $('#zudo-player-container .unfavorite').hide()

    if $('#favorites-page').length > 0
      $(this).closest('.songRow').remove()

    $.gritter.add
      image: '/assets/success.png'
      title: 'Success'
      text: data.message
    return
  .on 'ajax:error', '.unfavorite', (e, data, status, xhr) ->
    $(this).closest('span.favoriteProject').attr 'data-status', 'unfavorite'
    $.gritter.add
      image: '/assets/error.png'
      title: 'Error'
      text: data.message
    return

  $('#song-form').submit ->
    $('[type="submit"]').html('<img src="/white-loading.svg" style="vertical-align: middle;" /> <span style="line-height: 40px; vertical-align: middle;">Submit Song</span>').attr('disabled', 'disabled')
    return

  return