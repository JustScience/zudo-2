$ ->
  modal_holder_selector = '#modal-holder'
  modal_selector = '.reveal-modal'

  $(document).on 'click', 'a[data-modal]', ->
    location = $(this).attr('href')
    #Load modal dialog from server
    $.get location, (data)->
      $(modal_holder_selector).html(data).find(modal_selector).reveal()
      $ ->
        $('#tabs-1').tabs()
        return
      return

    false

  $(document).on 'click', '[data-must-be-logged-in]', ->
    location = '/must_login'
    $.get location, (data)->
      $(modal_holder_selector).html(data).find(modal_selector).reveal()
      return
    false

  $(document).on 'ajax:success', 'form[data-modal]', (event, data, status, xhr)->
    url = xhr.getResponseHeader('Location')
    if url
      # Redirect to url
      window.location = url

    else
      # Remove old modal backdrop
      $('.reveal-modal-bg').remove()

      # Replace old modal with new one
      $(modal_holder_selector).html(data).find(modal_selector).reveal()

    false

  return