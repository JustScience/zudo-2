$(document).ready(function() {
	$('.navBTN').click(function() {
		var nav = $('nav.main-nav');
		var icon = $('i#navicon')
		nav.fadeToggle();
		icon.toggleClass('ion-android-close');
		icon.toggleClass('ion-navicon');
	})
});