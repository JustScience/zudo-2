$('form').submit ->
  if $('#loading_image').length == 0
    $(':submit').before '<img src="/assets/loading.svg" style="display: none;" alt="loading" id="loading_image">'
  $('#loading_image').show()
  $(':submit', this).attr 'disabled', 'disabled'
  true