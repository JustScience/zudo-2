$(document).ready(function() {

	btnHover('.btn', 'rubberBand');
	animationHover('.more', 'animated tada');
	animationHover('#js--home-icon-1', 'tada');
	animationHover('#js--home-icon-2', 'tada');
	animationHover('#js--home-icon-3', 'tada');
	animationHover('#js--home-icon-4', 'tada');
	animationHover('#js--home-artist-1', 'pulse');
	animationHover('#js--home-artist-2', 'pulse');
	animationHover('#js--home-artist-3', 'pulse');

	function btnHover(element, animation){
		element = $(element);
		element.hover(
		   function() {
		      element.addClass(animation);
		      element.removeClass('shake');        
		   },
		   function(){
		      //wait for animation to finish before removing classes
		      window.setTimeout( function(){
		          element.removeClass(animation);
		          element.addClass('shake');
		      }, 2000);         
	      });
	}

	function animationHover(element, animation){
		element = $(element);
		element.hover(
		   function() {
		      element.addClass(animation);
		   },
		   function(){
		      //wait for animation to finish before removing classes
		      window.setTimeout( function(){
		          element.removeClass(animation);
		      }, 2000);         
	      });
	}

	/* SCROLL ON CLICK */
	$('.more').click(function() {
		$('html, body').animate({scrollTop: $('.js--wp-1').offset().top}, 1000)
	});

	/* SCROLL ANIMATIONS */
	$('.js--wp-1').waypoint(function(direction) {
		$('.js--wp-1').addClass('animated fadeIn');
	}, {
		offset: '55%'
	});

	$('.js--wp-2').waypoint(function(direction) {
		$('.js--wp-2').addClass('animated fadeInUp');
	}, {
		offset: '55%'
	});
	$('.js--wp-2-2').waypoint(function(direction) {
		$('.js--wp-2-2').addClass('animated slideInUp');
	}, {
		offset: '80%'
	});

	$('.js--wp-3').waypoint(function(direction) {
		$('.js--wp-3').addClass('animated zoomIn');
	}, {
		offset: '60%'
	});
	$('.js--wp-4').waypoint(function(direction) {
		$('.js--wp-4').addClass('animated fadeIn');
	}, {
		offset: '60%'
	});
	$('.js--wp-4-2').waypoint(function(direction) {
		$('.js--wp-4-2').addClass('animated bounceIn');
	}, {
		offset: '70%'
	});
});