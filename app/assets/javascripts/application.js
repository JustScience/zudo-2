// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui/tabs
//= require gritter
//= require jquery.mmenu.min.all
//= require jquery.tagsinput
//= require jquery.pjax
//= require underscore
//= require jquery.reveal
//= require jquery.jplayer
//= require_tree .
//= require turbolinks
//= require chosen-jquery


$(function(){

	//update play counts for songs
  $('.playCount').click(function(){
  	$('.songRow').removeClass('is-playing');
  	$(this).closest("tr").addClass('is-playing');
    song_id = $(this).attr("data-play-id");
    if(song_id){
	    $.ajax({
	      type: 'POST',
	      url: '/songs/'+song_id+'/update_play_count'
	    });
	  }
  });

  $('.noPlayCount').click(function(){
  	$('.songRow').removeClass('is-playing');
  	$(this).closest("tr").addClass('is-playing');
  });
});