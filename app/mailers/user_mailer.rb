class UserMailer < BaseMandrillMailer
  def welcome(user)
    subject = 'Welcome to Zudo!'
    merge_vars = {
        'NAME' => user.name
    }
    body = mandrill_template('welcome', merge_vars)

    send_mail(user.email, subject, body)
  end

  def submit_music(hash)
  	@hash = hash
  	mail(from: "info@zudomusic.com", to: 'submit@zudomusic.com', subject: "Zudo | New Artist Submission Request")
  end

  def payment_success(user_email, purchased_items)
    @user_email = user_email
    @purchased_items = purchased_items
    mail(from: "info@zudomusic.com", to: user_email, subject: "Your Licenses from Zudo Music")
  end

  def purchase_user_details(hash)
      @hash = hash
      mail(from: "info@zudomusic.com", to: 'info@zudomusic.com', subject: "Zudo | Purchase User Info")
  end

end