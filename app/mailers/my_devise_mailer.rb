require "mandrill"

class MyDeviseMailer < Devise::Mailer

	default(
    from: 'info@zudomusic.com',
    reply_to: 'info@zudomusic.com'
  )

  def confirmation_instructions(record, token, opts={})
    # code to be added here later
  end
  
  def reset_password_instructions(record, token, opts={})
  	subject = 'Reset password instructions'
    merge_vars = {
      'NAME' => record.name,
      'URL' => edit_user_password_url(reset_password_token: token)
    }
    body = mandrill_template('reset-password-instructions', merge_vars)

    send_mail(record.email, subject, body)
  end
  
  def unlock_instructions(record, token, opts={})
    # code to be added here later
  end

  private

  def send_mail(email, subject, body)
    mail(to: email, subject: subject, body: body, content_type: 'text/html')
  end

  def mandrill_template(template_name, attributes)
    mandrill = Mandrill::API.new(Rails.application.secrets.smtp_password)

    merge_vars = attributes.map do |key, value|
      { name: key, content: value }
    end

    mandrill.templates.render(template_name, [], merge_vars)['html']
  end
  
end

