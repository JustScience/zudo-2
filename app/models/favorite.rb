class Favorite < ActiveRecord::Base
  belongs_to :user
  belongs_to :favorited, counter_cache: true, polymorphic: true
end
