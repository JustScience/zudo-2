class ShoppingCart < ActiveRecord::Base
  has_many :shopping_cart_items

  def add(song, price, license)
    shopping_cart_items.create(song: song, price: price, license: license)
  end

  def clear
    shopping_cart_items.clear
  end

  def empty?
    shopping_cart_items.empty?
  end

  def total
      total_price =  shopping_cart_items.inject(Money.new(0)) { |sum, item| sum += (item.price) }
         if used_credits.present?
            total_price = total_price.to_f - used_credits.to_f
        end

        total_price.to_money
  end

   def sub_total
      shopping_cart_items.inject(Money.new(0)) { |sum, item| sum += (item.price) }
  end

end
