class Pace < ActiveRecord::Base
	default_scope { order(name: :asc) }
	mount_uploader :icon, PaceIconUploader

	has_many :songs
	
	validates :name, :desc, presence: true
end
