class Project < ActiveRecord::Base

	validates :name, presence: true
	validates :name, uniqueness: {scope: :user_id}

	belongs_to :user
	has_many :projects_songs

end