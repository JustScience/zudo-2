class Purchase < ActiveRecord::Base
  has_one :payment
  has_many :purchase_items
  belongs_to :user

  accepts_nested_attributes_for :purchase_items, :payment
end
