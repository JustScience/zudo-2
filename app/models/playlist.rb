class Playlist < ActiveRecord::Base
	mount_uploader :image, PlaylistImageUploader

  def to_param
    [id, name.parameterize, 'playlist'].join("-")
  end

  validates_processing_of :image
  validates :name, :image, presence: true

  belongs_to :user
  has_many :playlist_entries
  has_many :songs, through: :playlist_entries
end
