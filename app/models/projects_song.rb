class ProjectsSong < ActiveRecord::Base
	validates :project_id, presence: true

	belongs_to :user
	belongs_to :song

end