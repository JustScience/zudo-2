class Mood < ActiveRecord::Base
	default_scope { order(name: :asc) }
	has_many :songs
	
	validates :name, :desc, presence: true
end
