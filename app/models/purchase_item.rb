class PurchaseItem < ActiveRecord::Base
  belongs_to :purchase
  belongs_to :song

  monetize :price_cents

  default_value_for :downloads, 5
end
