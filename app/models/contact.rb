class Contact < MailForm::Base
  attribute :name
  attribute :email, validate: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message

  validates :name, presence: true
  validates :message, presence: true

  def headers
    {
        subject: 'Zudo Contact Form',
        to: 'contact@zudomusic.com',
        from: 'info@zudomusic.com'
    }
  end
end