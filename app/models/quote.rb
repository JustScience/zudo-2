class Quote < ActiveRecord::Base
	mount_uploader :image, QuoteImageUploader

	def to_param
		[id, title.parameterize].join("-")
	end

   	validates_integrity_of :image
   	validates_processing_of :image
   	validates :title, :content, presence: true

   	has_many :comments
end
