class Artist < ActiveRecord::Base
 	mount_uploader :image, ArtistImageUploader

  def to_param
    [id, name.parameterize].join("-")
  end

 	validates_integrity_of :image
 	validates_processing_of :image
 	validates :name, :user_id, :bio, :image, presence: true

 	has_many :songs, dependent: :destroy
 	has_many :similar_artists
 	belongs_to :user

 	scope :active, -> { where("active =  'true'" || "active = true").order('name ASC') }

 	def is_featured?
 		is_featured
 	end

 	def is_active?
 		active
 	end


end
