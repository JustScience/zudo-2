class License < ActiveHash::Base
    # KEEP TRACK OF ANY HISTORICALLY REMOVED LICENSE TIER WHEN CREATING NEW ONES
  self.data = [
    { id: 40, slug: 'tv-webisode-30', name: 'Webisode TV (<30 sec)', price: 129 },
    { id: 41, slug: 'tv-webisode-60', name: 'Webisode TV (<60 sec)', price: 159 },
    { id: 42, slug: 'tv-webisode-90', name: 'Webisode TV (<90 sec)', price: 189 },
    { id: 43, slug: 'tv-webisode-full', name: 'Webisode TV (>90 sec)', price: 219 }, # WAS ID 10
    { id: 44, slug: 'tv-webisode-credits', name: 'Webisode TV (Credits)', price: 279 }, # WAS ID 11
    { id: 45, slug: 'tv-webisode-theme', name: 'Webisode TV (Theme/Trailer)', price: 399 },

    { id: 50, slug: 'tv-network-30', name: 'Network TV (<30 sec)', price: 279 }, # WAS ID 12
    { id: 51, slug: 'tv-network-60', name: 'Network TV (<60 sec)', price: 399 }, # WAS ID 13
    { id: 52, slug: 'tv-network-90', name: 'Network TV (<90 sec)', price: 429 }, # WAS ID 14
    { id: 53, slug: 'tv-network-full', name: 'Network TV (>90 sec)', price: 699 }, # WAS ID 15
    { id: 54, slug: 'tv-network-credits', name: 'Network TV (Credits)', price: 999 }, # WAS ID 16
    { id: 55, slug: 'tv-network-theme', name: 'Network TV (Theme/Trailer)', price: 1299 },

    { id: 61, slug: 'film-indie-fest', name: 'Indie Film Festival (No Web)', price: 159 }, # WAS ID 4
    { id: 62, slug: 'film-indie-fest-web', name: 'Indie Film Festival + Web', price: 219 }, 
    { id: 63, slug: 'film-indie-fest-credits', name: 'Indie Film Fest (Credits)', price: 249 }, # WAS ID 5
    { id: 64, slug: 'film-indie-fest-trailer', name: 'Indie Film Fest (Trailer)', price: 279 },

    { id: 65, slug: 'film-indie-dist', name: 'Indie Film w/ Distribution', price: 399 }, # WAS ID 6
    { id: 66, slug: 'film-indie-dist-credits', name: 'Indie Film w/ Distribution (Credits)', price: 519 }, # WAS ID 7
    { id: 67, slug: 'film-indie-dist-trailer', name: 'Indie Film w/ Distribution (Trailer)', price: 579 },

    { id: 70, slug: 'film-studio', name: 'Studio Film', price: 999 }, # WAS ID 8
    { id: 71, slug: 'film-studio-credits', name: 'Studio Film (Credits)', price: 1299 }, # WAS ID 9
    { id: 72, slug: 'film-studio-trailer', name: 'Studio Film (Trailer)', price: 1599 },

    { id: 80, slug: 'video-personal', name: 'Personal Web Video', price: 39 },
    { id: 81, slug: 'video-professional-web', name: 'Professional Video (Web Only)', price: 69 },
    { id: 82, slug: 'video-professional-print', name: 'Professional Video (Web + Print)', price: 99 },

    { id: 90, slug: 'video-crowdfund-5k', name: 'Crowdfunding (<5k goal)', price: 69 },
    { id: 91, slug: 'video-crowdfund-20k', name: 'Crowdfunding (<20k goal)', price: 189 }, # WAS ID 26
    { id: 92, slug: 'video-crowdfund-50k', name: 'Crowdfunding (<50k goal)', price: 369 }, # WAS ID 27
    { id: 93, slug: 'video-crowdfund-80k', name: 'Crowdfunding (<80k goal)', price: 489 }, # WAS ID 28
    { id: 94, slug: 'video-crowdfund-100k', name: 'Crowdfunding (<100k goal)', price: 579 }, # WAS ID 29
    { id: 95, slug: 'video-crowdfund-xl', name: 'Crowdfunding (>100k goal)', price: 729 }, # WAS ID 30
    
    { id: 100, slug: 'youtube', name: 'YouTube Video (< 1k Subscribers)', price: 24 },
    { id: 101, slug: 'youtube-1k', name: 'YouTube Video (> 1k Subscribers)', price: 39 },
    { id: 102, slug: 'youtube-10k', name: 'YouTube Video (> 10k Subscribers)', price: 69 },
    { id: 103, slug: 'youtube-100k', name: 'YouTube Video (> 100k Subscribers)', price: 129 },
    { id: 104, slug: 'youtube-1M', name: 'YouTube Video (> 1M Subscribers)', price: 219 },
    { id: 105, slug: 'youtube-theme', name: 'YouTube Video (< 10k Subscribers)', price: 189 },
    { id: 106, slug: 'youtube-theme-10k', name: 'YouTube Video (> 10k Subscribers)', price: 399 },
    { id: 107, slug: 'youtube-theme-100k', name: 'YouTube Video (> 100k Subscribers)', price: 549 },

    { id: 110, slug: 'podcast', name: 'Podcast (< 1k Subscribers)', price: 24 },
    { id: 111, slug: 'podcast-1k', name: 'Podcast (> 1k Subscribers)', price: 39 },
    { id: 112, slug: 'podcast-10k', name: 'Podcast (> 10k Subscribers)', price: 69 },
    { id: 113, slug: 'podcast-100k', name: 'Podcast (> 100k Subscribers)', price: 129 },
    { id: 114, slug: 'podcast-1M', name: 'Podcast (> 1M Subscribers)', price: 219 },
    { id: 115, slug: 'podcast-theme', name: 'Podcast (< 10k Subscribers)', price: 189 },
    { id: 116, slug: 'podcast-theme-10k', name: 'Podcast (> 10k Subscribers)', price: 399 },
    { id: 117, slug: 'podcast-theme-100k', name: 'Podcast (> 100k Subscribers)', price: 549 },

    { id: 120, slug: 'app-free', name: 'Mobile App (Free)', price: 129 }, # WAS ID 19
    { id: 121, slug: 'app-paid', name: 'Mobile App (Paid)', price: 249 }, # WAS ID 20
    { id: 122, slug: 'app-subscription', name: 'Mobile App (Subscription)', price: 339 },

    { id: 130, slug: 'game-indie-pc', name: 'Indie PC Game', price: 99 },
    { id: 131, slug: 'game-indie-mobile', name: 'Indie Mobile Game', price: 129 },
    { id: 132, slug: 'game-indie-console', name: 'Indie Console Game', price: 189 },

    { id: 133, slug: 'game-studio-pc', name: 'Studio PC Game', price: 279 },
    { id: 134, slug: 'game-studio-mobile', name: 'Studio Mobile Game', price: 399 },
    { id: 135, slug: 'game-studio-console', name: 'Studio Console Game', price: 699 },

    { id: 140, slug: 'ads-local-30', name: 'Local Advertising (<30 sec)', price: 399 },
    { id: 141, slug: 'ads-local-60', name: 'Local Advertising (<60 sec)', price: 549 },
    { id: 142, slug: 'ads-local-90', name: 'Local Advertising (>60 sec)', price: 999 },

    { id: 143, slug: 'ads-national-30', name: 'National Advertising (<30 sec)', price: 999 },
    { id: 144, slug: 'ads-national-60', name: 'National Advertising (<60 sec)', price: 1299 },
    { id: 145, slug: 'ads-national-90', name: 'National Advertising (>60 sec)', price: 1599 },

    { id: 146, slug: 'ads-non-profit-local', name: 'Local Non Profit Ad', price: 129 }, # WAS ID 31
    { id: 147, slug: 'ads-non-profit-regional', name: 'Regional Non Profit Ad', price: 299 }, # WAS ID 32
    { id: 148, slug: 'ads-non-profit-national', name: 'National Non Profit Ad', price: 549 }, # WAS ID 33
    { id: 149, slug: 'ads-non-profit-international', name: 'International Non Profit Ad', price: 699 }, # WAS ID 34

    { id: 150, slug: 'corporate-event-100', name: 'Corporate (<100 employees)', price: 189 },
    { id: 151, slug: 'corporate-event-1k', name: 'Corporate (<1k employees)', price: 369 },
    { id: 152, slug: 'corporate-event-2500', name: 'Corporate (<2500 employees)', price: 579 },
    { id: 153, slug: 'corporate-event-5k', name: 'Corporate (<5k employees)', price: 729 },
    { id: 154, slug: 'corporate-event-xl', name: 'Corporate (>5k employees)', price: 999 },
 
    { id: 160, slug: 'film-student-project', name: 'Student Film Project (no Web)', price: 24 },
    { id: 161, slug: 'film-student-festival', name: 'Student Film Festival (no Web)', price: 69 },
    # { id: 162, slug: 'student-film-coursework', name: 'Student Film Course Material', price: XX },

    { id: 163, slug: 'educational-video-30', name: 'Educational-Use Video (<30 sec)(*limited web)', price: 39 },
    { id: 164, slug: 'educational-video-90', name: 'Educational-Use Video (<90 sec)(*limited web)', price: 69 },
    { id: 165, slug: 'educational-video-full', name: 'Educational-Use Video (>90 sec)(*limited web)', price: 99 },


    ###
    # REMOVED and REPLACED LICENSES BELOW
    ###
    { id: 1, slug: 'video-home', name: 'Home Video', price: 39 }, # REMOVED
    { id: 2, slug: 'video-web', name: 'Web Video', price: 39 }, # REPLACED
    { id: 3, slug: 'video-professional', name: 'Professional Video (Web Only)', price: 69 }, # REPLACED
    { id: 4, slug: 'film-indie-fest', name: 'Indie Film Festivals & Web', price: 189 }, # REPLACED
    { id: 5, slug: 'film-indie-fest-credits', name: 'Indie Film Fest (Credits)', price: 249 }, # REPLACED
    { id: 6, slug: 'film-indie-dist', name: 'Indie Film w/ Distribution', price: 429 }, # REPLACED
    { id: 7, slug: 'film-indie-dist-credits', name: 'Indie Film w/ Distribution (Credits)', price: 519 }, # REPLACED
    { id: 8, slug: 'film-studio', name: 'Studio Film', price: 759 }, # REPLACED
    { id: 9, slug: 'film-studio-credits', name: 'Studio Film (Credits)', price: 1299 }, # REPLACED
    { id: 10, slug: 'tv-webisode-full', name: 'Webisode TV (>90 sec)', price: 219 }, # REPLACED
    { id: 11, slug: 'tv-webisode-credits', name: 'Webisode TV (Credits)', price: 279 }, # REPLACED
    { id: 12, slug: 'tv-network-30', name: 'Network TV (<30 sec)', price: 279 }, # REPLACED
    { id: 13, slug: 'tv-network-60', name: 'Network TV (<60 sec)', price: 399 }, # REPLACED
    { id: 14, slug: 'tv-network-90', name: 'Network TV (<90 sec)', price: 429 }, # REPLACED
    { id: 15, slug: 'tv-network-full', name: 'Network TV (>90 sec)', price: 699 }, # REPLACED
    { id: 16, slug: 'tv-network-credits', name: 'Network TV (Credits)', price: 999 }, # REPLACED
    { id: 17, slug: 'game-indie', name: 'Indie Video Game', price: 199 }, # REMOVED
    { id: 18, slug: 'game-console', name: 'Console Video Game', price: 599 }, # REMOVED
    { id: 19, slug: 'app-free', name: 'Free Mobile App', price: 129 }, # REPLACED
    { id: 20, slug: 'app-paid', name: 'Paid Mobile App', price: 279 }, # REPLACED
    { id: 21, slug: 'small-business-1-20-employees', name: '1-20 Employees', price: 189 }, # REMOVED
    { id: 22, slug: 'small-business-21-50-employees', name: '21-50 Employees', price: 369 }, # REMOVED
    { id: 23, slug: 'small-business-51-100-employees', name: '51-100 Employees', price: 579 }, # REMOVED
    { id: 24, slug: 'small-business-101-250-employees', name: '101-250 Employees', price: 729 }, # REMOVED
    { id: 25, slug: 'small-business-251-employees', name: '> 251 Employees', price: 969 }, # REMOVED
    { id: 26, slug: 'crowdfunding-20k-fundraising-goal', name: '< $20k Fundraising Goal', price: 189 }, # REPLACED
    { id: 27, slug: 'crowdfunding-21-50k-fundraising-goal', name: '$21-50k Fundraising Goal', price: 369 }, # REPLACED
    { id: 28, slug: 'crowdfunding-51-80k-fundraising-goal', name: '$51-80k Fundraising Goal', price: 579 }, # REPLACED
    { id: 29, slug: 'crowdfunding-81-100k-fundraising-goal', name: '$80-100k Fundraising Goal', price: 729 }, # REPLACED
    { id: 30, slug: 'crowdfunding-100k-fundraising-goal', name: '> $100k Fundraising Goal', price: 969 }, # REPLACED
    { id: 31, slug: 'non-profit-local', name: 'Local Non Profit', price: 129 }, # REPLACED
    { id: 32, slug: 'non-profit-regional', name: 'Regional Non Profit', price: 299 }, # REPLACED
    { id: 33, slug: 'non-profit-national', name: 'National Non Profit', price: 499 }, # REPLACED
    { id: 34, slug: 'non-profit-international', name: 'International Non Profit', price: 699 }, # REPLACED
 ]
end

