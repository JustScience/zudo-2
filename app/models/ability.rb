class Ability
  include CanCan::Ability
  
  def initialize(user)
    user ||= User.new # guest user
 
    if user.admin?
      can :manage, :all
    elsif user.moderator?
      can :manage, Song
      can :manage, Artist
      can :manage, Playlist
    elsif user.supervisor?
      can :read, :all
    elsif user.artist?
      can :create, Song
      can :manage, Song do |song|
        song.try(:user) == user
      end
      can :manage, Artist do |artist|
        artist.try(:user_id) == user.id
      end
      can :placements, PurchaseItem
    else
      can :create, User
      can :update, User
      can :read, :all
      can :new_artist_submission, Artist
    end
  end
end