class Post < ActiveRecord::Base
	mount_uploader :image, PostImageUploader

	def to_param
		[id, title.parameterize].join("-")
	end

   	validates_integrity_of :image
   	validates_processing_of :image
   	validates :title, :body, :author, :desc, :keyword, presence: true

   	has_many :comments
end
