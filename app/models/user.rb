class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true

  has_many :songs, dependent: :destroy
  has_many :favorites
  has_many :favorite_projects, through: :favorites, source: :favorited, source_type: 'Song'
  has_many :artists
  has_many :purchases

  has_many :playlist_entries
  has_many :playlists, through: :playlist_entries
  has_many :projects, dependent: :destroy

  after_create do
    begin
        UserMailer.welcome(self).deliver_now
        gibbon = Gibbon::Request.new(api_key: Rails.application.secrets.mailchimp_api_key)
        gibbon.lists(Rails.application.secrets.mailchimp_list_id).members.create(body: {email_address: self.email, status: 'subscribed', merge_fields: {FNAME: self.first_name, LNAME: self.last_name}})
      rescue Gibbon::MailChimpError => exception
          Rails.logger.error("failed becuz #{exception.detail}")
      end
  end

  def first_name
    if name.split.count > 1
      name.split.first
    else
      name
    end
  end

  def last_name
    if name.split.count > 1
      name.split.last
    else
      first_name
    end
  end

  #def 

end
