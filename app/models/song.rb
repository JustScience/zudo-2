class Song < ActiveRecord::Base
  #default_scope { order('random()') }
  include PgSearch
  multisearchable against: [:title, :desc], using: { tsearch: {any_word: true, prefix: true } , trigram: {:threshold => 0.1}}, if: :approved?
  mount_uploader :image, SongImageUploader
  mount_uploader :sample, SongSampleUploader
  paginates_per 20

  after_save :update_pg_search
  
  def approved?
    approved
  end

  def to_param
    [id, artist.name.parameterize, title.parameterize].join("-")
  end

  validates_integrity_of :image
  validates_processing_of :image
  validates :image, presence: true

  validates_integrity_of :sample
  validates_processing_of :sample
  validates :sample, presence: true

  validates :title, :desc, :genre, :mood, :pace, :era, :key, :tempo, :instrument, presence: true

  belongs_to :user
  belongs_to :artist
  belongs_to :genre
  belongs_to :mood
  belongs_to :pace
  belongs_to :era
  belongs_to :key
  belongs_to :tempo
  belongs_to :instrument

  has_many :playlist_entries
  has_many :playlists, through: :playlist_entries
  has_many :favorites, as: :favorited

  private

    def update_pg_search
      self.pg_search_document.update_columns(artist_id: artist_id, pace_id: pace_id, genre_id: genre_id, mood_id: mood_id, tempo_id: tempo_id, instrument_id: instrument_id, era_id: era_id, key_id: key_id) if self.pg_search_document.present? && approved
    end
end

