if Rails.env.test?
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = false
  end
else
  CarrierWave.configure do |config|
    config.storage = :aws
    #config.cache_storage = :aws
    config.aws_credentials = {
      access_key_id:     Rails.application.secrets.aws_access_key_id,
      secret_access_key: Rails.application.secrets.aws_secret_access_key,
      region:            Rails.application.secrets.aws_region
    }
    config.aws_acl       = :public_read
    config.aws_bucket    = Rails.application.secrets.s3_bucket_name
    config.store_dir     = nil # store at the root level
    config.cache_dir     = 'cache'
  end
end