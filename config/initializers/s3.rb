CarrierWave.configure do |config|
  config.cache_dir = "#{Rails.root}/tmp/uploads"
  config.storage = :aws
  config.aws_bucket = Rails.application.secrets.s3_bucket_name
  config.aws_acl = :'public-read'
  config.aws_authenticated_url_expiration = 60 * 60 * 24

  config.aws_credentials = {
      access_key_id: Rails.application.secrets.aws_access_key_id,
      secret_access_key: Rails.application.secrets.aws_secret_access_key,
      region: Rails.application.secrets.aws_region
  }
end