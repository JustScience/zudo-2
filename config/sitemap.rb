# Set the host name for URL creation
require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = "http://www.zudomusic.com"
SitemapGenerator::WaveAdapter.new
SitemapGenerator::Sitemap.adapter = SitemapGenerator::S3Adapter.new(fog_provider: 'AWS',
                                   aws_access_key_id: Rails.application.secrets.aws_access_key_id,
                                   aws_secret_access_key: Rails.application.secrets.aws_secret_access_key,
                                   fog_directory: Rails.application.secrets.s3_bucket_name,
                                   fog_region: Rails.application.secrets.aws_region)
SitemapGenerator::Sitemap.sitemaps_host = "http://zudosongs.s3.amazonaws.com/"
# pick a safe place safe to write the files
SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  add home_path, priority: 0.7
  add about_path, priority: 0.6
  add play_path, priority: 0.8
  add artists_path, priority: 0.7,changefreq: 'weekly'
  add playlists_path, priority: 0.7,changefreq: 'weekly'
  add songs_path, priority: 0.4,changefreq: 'daily'
  add new_contact_path, priority: 0.3
  add faq_path, priority: 0.2
  add terms_path, priority: 0.2
  add new_user_registration_path, priority: 0.1
  add new_user_session_path, priority: 0.1
  add dabooth_path, priority: 0.0
  add submit_your_music_path, priority: 0.0
  Artist.find_each do |artist|
    add artist_path(artist), lastmod: artist.updated_at, priority: 1.0
  end
  Playlist.find_each do |playlist|
    add playlist_path(playlist), lastmod: playlist.updated_at, priority: 1.0
  end
  add usermod_path, priority: 0.0
  add studio_path, priority: 0.0
  add must_login_path, priority: 0.0
  add share_zudo_path, priority: 0.0
  add studio_path, priority: 0.0
end

SitemapGenerator::Sitemap.ping_search_engines