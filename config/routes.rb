Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }
  resources :users
  resources :instruments, only: [:new, :edit, :create, :update, :destroy]
  resources :tempos, only: [:new, :edit, :create, :update, :destroy]
  resources :keys, only: [:new, :edit, :create, :update, :destroy]
  resources :eras, only: [:new, :edit, :create, :update, :destroy]
  resources :paces, only: [:new, :edit, :create, :update, :destroy]
  resources :moods, only: [:new, :edit, :create, :update, :destroy]
  resources :genres, only: [:new, :edit, :create, :update, :destroy]
  resources :artists
  resources :songs
  resources :projects
  get 'projects/add_song/:song_id' => 'projects#add_song', as: :add_song_project
  post 'projects/submit_song/:song_id' => 'projects#submit_song', as: :submit_song_project
  get 'edit_song_project/:id' => 'projects#edit_song', as: :edit_song_project
  delete 'projects/delete_song/:id' => 'projects#delete_song', as: :delete_song_project
  post 'projects/submit_project' => 'projects#submit_project', as: :submit_project
  resources :favorite_projects, only: [:create, :destroy, :playlist]
  resources :quotes do
    member do
      get :share
    end
  end
  resources :posts do
    resources :comments
  end
  resources :songs do
    member do
      get :download
      get :share
      get :add_to_cart
      post :add_to_cart
      post 'add_song'
      post 'remove_song'
      post :update_play_count
    end
  end
  resources :playlists do
    member do
      post 'add_song'
      post 'remove_song'
    end
  end
  resources :contacts, only: [:new, :create]

  resources :shopping_cart_items, only: [:destroy]
  
  get '/cart', to: 'shopping_cart_items#index'

  post '/checkout', to: 'shopping_cart_items#checkout'

  get '/purchases', to: 'purchase_items#index'
  get '/placements', to: 'purchase_items#placements'

  get '/purchased_items/:id/download', to: 'purchase_items#download', as: :download

  get '/home', to:  'pages#home'
  get '/credits', to: 'users#user_credits'
  get '/new_credit', to: 'users#new_credit'
  post '/submit_credit', to: 'users#submit_credit'

  get '/usermod', to:  'users#admin'

  get '/relationships', to:  'artists#relationships'
  post '/save_similar_artist/:id', to:  'artists#save_similar_artist', as: :save_similar_artist
  post '/artists/save_featured/:id', to: 'artists#save_featured'
  post '/artists/save_as_active/:id', to: 'artists#save_as_active'
  match "/submit_your_music" => "artists#new_artist_submission", as: :submit_your_music, via: [:get, :post]
  get '/about', to:  'pages#about'

  get '/contact', to: 'contacts#new'

  get '/film', to:  'pages#film'

  get '/horror', to:  'pages#horror'

  get '/games', to:  'pages#games'

  get '/ads', to:  'pages#ads'

  get '/video', to:  'pages#video'

  get '/blog', to:  'posts#index'

  get '/terms', to:  'pages#terms'

  get '/submit', to:  'artists#new_artist_submission'

  get '/faq', to:  'pages#faq'

  get '/play', to: 'songs#library'

  get '/favorites', to: 'songs#favorite'

  get '/dabooth', to: 'pages#admin'

  get '/teamzudosongadminpage', to: 'songs#admin'
  get '/teamzudosongapprovalpage', to: 'songs#approvals'
  post '/approve_deny_song/:id', to: 'songs#approve_deny', as: :approve_deny_song
  get '/denied-songs', to: 'songs#denied_songs', as: :denied_songs
  get '/deny_song/:id', to: 'songs#deny_song', as: :deny_song

  get '/sorry', to: 'pages#deny'

  get '/studio', to: 'songs#seller'

  get '/romance', to: 'pages#romance'



  as :user do
    get "/login" => "devise/sessions#new"
    get '/signup', to: 'registrations#new'
  end

  get '/share_zudo', to: 'site#share_zudo', as: :share_zudo

  get '/must_login', to: 'site#must_login', as: :must_login

  root 'pages#home'

  get '/genres', to: redirect('/play')
  get '/genres/:id', to: redirect('/play')
  get '/moods', to: redirect('/play')
  get '/moods/:id', to: redirect('/play')
  get '/paces', to: redirect('/play')
  get '/paces/:id', to: redirect('/play')
  get '/eras', to: redirect('/play')
  get '/eras/:id', to: redirect('/play')
  get '/keys', to: redirect('/play')
  get '/keys/:id', to: redirect('/play')
  get '/tempos', to: redirect('/play')
  get '/tempos/:id', to: redirect('/play')
  get '/instruments', to: redirect('/play')
  get '/instruments/:id', to: redirect('/play')

end
